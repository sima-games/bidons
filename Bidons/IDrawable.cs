﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bidons
{
	public interface IDrawable
	{
		void Draw();
		void DrawAt(double x, double y);
	}
}
