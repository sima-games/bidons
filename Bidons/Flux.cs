﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Reflection;
//using System.Text;
//using System.Threading.Tasks;

//namespace Bidons
//{
//	static class FluxExtensions
//	{
//		public static Func<dynamic, dynamic> GetEase(this List<Flux.Ease> eases, string easeName)
//		{
//			return eases.First(f => f.Name == easeName)?.Func;
//		}
//	}

//	public class Flux
//	{
//		public abstract class Ease
//		{
//			public string Name { get; set; }

//			public abstract object Func(object number);

//			public Func<object, object> Get() => Func;
//			public Func<object, object> GetIn() => Func;
//			public Func<object, object> GetInOut() => Func;

//			public Ease(string name, Func<dynamic, dynamic> func)
//			{
//				Name = name;
//			}

//			public class Linear : Ease
//			{
//				public override object Func(object number)
//				{
//					throw new NotImplementedException();
//				}
//			}
//		}

//		public const string FluxVersion = "0.1.5";
//		List<Tween> _tweens = new List<Tween>();

//		List<Ease> Easing = new List<Ease>
//		{
//			new Ease("Linear", new Func<dynamic,dynamic>((dynamic p) => p)),
//			new Ease("Quad", new Func<dynamic,dynamic>((dynamic p) => (dynamic)Math.Pow(p, 2))),
//			//new Ease("Cubic", new Func<dynamic,dynamic>((dynamic p) => Math.Pow(p, 3))),
//			//new Ease("Quart", new Func<dynamic,dynamic>((dynamic p) => Math.Pow(p, 4))),
//			//new Ease("Quint", new Func<dynamic,dynamic>((dynamic p) => Math.Pow(p, 5))), // "p * p * p * p * p",
//			//new Ease("Expo", new Func<dynamic,dynamic>((dynamic p) => Math.Pow(2, 10 * (p - 1)))), //"2 ^ (10 * (p - 1))",
//			//new Ease("Sine", new Func<dynamic,dynamic>((dynamic p) => -Math.Cos(p * (Math.PI * 0.5)) + 1)), // " - math.cos(p * (math.pi * .5)) + 1",
//			//new Ease("Circ", new Func<dynamic,dynamic>((dynamic p) => -Math.Sqrt(1 - (p * p)) - 1)), // " - (math.sqrt(1 - (p * p)) - 1)",
//			//new Ease("Back", new Func<dynamic,dynamic>((dynamic p) => p * p * (2.7 * p - 1.7))), //"p * p * (2.7 * p - 1.7)",
//			//new Ease("Elastic", new Func<dynamic,dynamic>((dynamic p) => -(Math.Pow(2, 10 * (p - 1)) * Math.Sin((p - 1.075) * (Math.PI * 2) / 0.3)))) //" - (2^(10 * (p - 1)) * math.sin((p - 1.075) * (math.pi * 2) / .3))"
//		};

//		public Flux()
//		{
//			foreach (var item in Easing)
//			{
//				Easing.Add(new Ease("in" + item.Name, item.Func));
//				Easing.Add(new Ease("out" + item.Name, (dynamic p) => 1 - item.Func(1 - p)));
//				Easing.Add(new Ease("inOut" + item.Name, (dynamic p) =>
//				{
//					p *= 2;
//					if (p < 1)
//						return 0.5 * item.Func(p);
//					p = 2 - p;
//					return 0.5 * (1 - item.Func(p)) + 0.5;
//				}));
//			}
//		}

//		public void Update()
//		{

//		}

//		private void clear(dynamic obj, dynamic vars)
//		{

//		}

//		public Tween To(dynamic obj, double time, dynamic vars)
//		{
//			var tween = new Tween(this, obj, time, vars);
//			_tweens.Add(tween);
//			return tween;
//		}

//		public class Tween
//		{
//			Flux _flux;
//			Func<dynamic, dynamic> _ease;
//			double _delay;
//			dynamic _obj;
//			double _rate;
//			dynamic _vars;
//			bool _inited = false;
//			private static bool _isNumericType(object o)
//			{
//				switch (o)
//				{
//					case byte _:
//					case sbyte _:
//					case ushort _:
//					case uint _:
//					case ulong _:
//					case short _:
//					case int _:
//					case long _:
//					case decimal _:
//					case double _:
//					case float _:
//						return true;
//					default:
//						return false;
//				}
//			}

//			public Tween(Flux flux, dynamic obj, double time, dynamic vars)
//			{
//				_flux = flux;
//				_obj = obj;
//				_rate = time > 0 ? (1 / time) : 0;
//				_delay = 0;
//				_ease = flux.Easing.GetEase("outQuad");

//				_vars = new { };
//				foreach (KeyValuePair<string, dynamic> item in vars)
//				{
//					if (!_isNumericType(item.Value))
//						throw new ArgumentException($"Bad value for key {item.Key}; expected number");
//					_vars[item.Key] = item.Value;
//				}
//			}

//			public Tween Ease(string easingType)
//			{
//				if (!_flux.Easing.Any(a => a.Name == easingType))
//					throw new KeyNotFoundException($"Bad easing type {easingType}");
//				_ease = _flux.Easing.GetEase(easingType);
//				return this;
//			}

//			public Tween Delay(double delay)
//			{
//				_delay = delay;
//				return this;
//			}

//			public Tween OnStart()
//			{
//				return this;
//			}

//			public Tween OnComplete()
//			{
//				return this;
//			}

//			public Tween OnUpdate()
//			{
//				return this;
//			}

//			private Tween init()
//			{
//				foreach (KeyValuePair<string, dynamic> item in _vars)
//				{
//					dynamic x = _obj[item.Key];
//					if (!_isNumericType(x))
//						throw new ArgumentException($"Bad value on object key {item.Key}; expected number");
//					_vars[item.Key] = new { start = x, diff = item.Value - x };
//				}
//				_inited = true;
//				return this;
//			}

//		}
//	}
//}

////		function tween:after(...)
////  local t
////  if select("#", ...) == 2 then
////	t = tween.new(self.obj, ...)
////  else
////	t = tween.new(...)
////  end
////  t.parent = self.parent
////  self:oncomplete(function() flux.add(self.parent, t) end)
////  return t
////end


////function tween:stop()
////  flux.remove(self.parent, self)
////end



////function flux.group()
////  return setmetatable({ }, flux)
////end


////function flux:to(obj, time, vars)
////  return flux.add(self, tween.new(obj, time, vars))
////end


////function flux:update(deltatime)
////  for i = #self, 1, -1 do
////	local t = self[i]
////	if t._delay > 0 then
////	  t._delay = t._delay - deltatime
////	else
////	  if not t.inited then
////		flux.clear(self, t.obj, t.vars)
////		t:init()
////	  end
////	  if t._onstart then
////		t._onstart()
////		t._onstart = nil
////	  end
////	  t.progress = t.progress + t.rate * deltatime
////	  local p = t.progress
////	  local x = p >= 1 and 1 or flux.easing[t._ease](p)
////	  for k, v in pairs(t.vars) do
////		t.obj[k] = v.start + x * v.diff
////	  end
////	  if t._onupdate then t._onupdate() end
////	  if p >= 1 then
////		flux.remove(self, i)
////		if t._oncomplete then t._oncomplete() end
////	  end
////	end
////  end
////end


////function flux:clear(obj, vars)
////  for t in pairs(self[obj]) do
////	if t.inited then
////	  for k in pairs(vars) do t.vars[k] = nil end
////	end
////  end
////end


////function flux:add(tween)
////  -- Add to object table, create table if it does not exist
////  local obj = tween.obj
////  self[obj] = self[obj] or { }
////		self[obj][tween] = true
////  -- Add to array
////  table.insert(self, tween)
////  tween.parent = self
////  return tween
////end


////function flux:remove(x)
////  if type(x) == "number" then
////	-- Remove from object table, destroy table if it is empty
////	local obj = self[x].obj
////	self[obj][self[x]] = nil
////	if not next(self[obj]) then self[obj] = nil end
////	-- Remove from array
////	self[x] = self[#self]
////	return table.remove(self)
////  end
////  for i, v in ipairs(self) do
////	if v == x then
////	  return flux.remove(self, i)
////	end
////  end
////end



////local bound = {
////  to = function(...) return flux.to(flux.tweens, ...) end,
////  update = function(...) return flux.update(flux.tweens, ...) end,
////  remove = function(...) return flux.remove(flux.tweens, ...) end,
////}
////	setmetatable(bound, flux)

////return bound

////}
////}
