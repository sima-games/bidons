﻿using Bidons.Base;
using Bidons.Core;
using Bidons.Elements;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyGame
{
	class SimpleScene : Scene
	{
		public SimpleScene()
		{
			Elements.Add(new Circle { Location = (Location) new[] { 0, 0 }, Radius = 25, Color = Color.Blue });
            Elements.Add(new Circle { Location = (Location) new[] { 100, 150 }, Radius = 35, Color = Color.Red });
            Elements.Add(new Label { Text = "Game Over!", Color = Color.Red });
        }
	}
}
