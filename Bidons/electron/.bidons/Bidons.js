/**
 * @version 1.0.0.0
 * @copyright Copyright ©  2019
 * @compiler Bridge.NET 17.7.0
 */
Bridge.assembly("Bidons", function ($asm, globals) {
    "use strict";

    Bridge.define("Bidons.Base.Anchor", {
        $kind: "struct",
        statics: {
            methods: {
                op_Implicit$2: function (tuple) {
                    return new Bidons.Base.Anchor.$ctor1(tuple.Item1, tuple.Item2);
                },
                op_Implicit$3: function (tuple) {
                    return new Bidons.Base.Anchor.$ctor1(tuple.Item1, tuple.Item2);
                },
                op_Implicit$1: function (array) {
                    if (array.length !== 2) {
                        throw new System.InvalidCastException.$ctor1("Cannot convert int[] to Anchor, wrong array length");
                    }
                    return new Bidons.Base.Anchor.$ctor1(array[System.Array.index(0, array)], array[System.Array.index(1, array)]);
                },
                op_Implicit: function (array) {
                    if (array.length !== 2) {
                        throw new System.InvalidCastException.$ctor1("Cannot convert double[] to Anchor, wrong array length");
                    }
                    return new Bidons.Base.Anchor.$ctor1(array[System.Array.index(0, array)], array[System.Array.index(1, array)]);
                },
                getDefaultValue: function () { return new Bidons.Base.Anchor(); }
            }
        },
        fields: {
            X: 0,
            Y: 0
        },
        ctors: {
            $ctor1: function (x, y) {
                this.$initialize();
                this.X = x;
                this.Y = y;
            },
            ctor: function () {
                this.$initialize();
            }
        },
        methods: {
            getHashCode: function () {
                var h = Bridge.addHash([1751376048, this.X, this.Y]);
                return h;
            },
            equals: function (o) {
                if (!Bridge.is(o, Bidons.Base.Anchor)) {
                    return false;
                }
                return Bridge.equals(this.X, o.X) && Bridge.equals(this.Y, o.Y);
            },
            $clone: function (to) {
                var s = to || new Bidons.Base.Anchor();
                s.X = this.X;
                s.Y = this.Y;
                return s;
            }
        }
    });

    Bridge.define("Bidons.Base.Color", {
        $kind: "struct",
        statics: {
            fields: {
                Red: null,
                Green: null,
                Blue: null,
                Black: null,
                White: null
            },
            ctors: {
                init: function () {
                    this.Red = new Bidons.Base.Color();
                    this.Green = new Bidons.Base.Color();
                    this.Blue = new Bidons.Base.Color();
                    this.Black = new Bidons.Base.Color();
                    this.White = new Bidons.Base.Color();
                    this.Red = new Bidons.Base.Color.$ctor1(16711680);
                    this.Green = new Bidons.Base.Color.$ctor1(65280);
                    this.Blue = new Bidons.Base.Color.$ctor1(255);
                    this.Black = new Bidons.Base.Color.$ctor1(0);
                    this.White = new Bidons.Base.Color.$ctor1(16777215);
                }
            },
            methods: {
                op_Implicit: function (colorNumber) {
                    return new Bidons.Base.Color.$ctor1(colorNumber);
                },
                getDefaultValue: function () { return new Bidons.Base.Color(); }
            }
        },
        fields: {
            R: 0,
            G: 0,
            B: 0,
            A: 0,
            ColorNumber: 0
        },
        ctors: {
            $ctor2: function (stringHex) {
                Bidons.Base.Color.$ctor1.call(this, System.Convert.toNumberInBase(System.String.replaceAll(stringHex, "#", ""), 16, 9));
            },
            $ctor1: function (colorNumber) {
                this.$initialize();
                this.ColorNumber = colorNumber;
                var values = System.BitConverter.getBytes$4(colorNumber);
                if (!System.BitConverter.isLittleEndian) {
                    System.Array.reverse(values);
                }
                this.R = values[System.Array.index(2, values)];
                this.G = values[System.Array.index(1, values)];
                this.B = values[System.Array.index(0, values)];
                this.R /= 255;
                this.G /= 255;
                this.B /= 255;
                this.A = 1;
            },
            ctor: function () {
                this.$initialize();
            }
        },
        methods: {
            getHashCode: function () {
                var h = Bridge.addHash([1869377461, this.R, this.G, this.B, this.A, this.ColorNumber]);
                return h;
            },
            equals: function (o) {
                if (!Bridge.is(o, Bidons.Base.Color)) {
                    return false;
                }
                return Bridge.equals(this.R, o.R) && Bridge.equals(this.G, o.G) && Bridge.equals(this.B, o.B) && Bridge.equals(this.A, o.A) && Bridge.equals(this.ColorNumber, o.ColorNumber);
            },
            $clone: function (to) {
                var s = to || new Bidons.Base.Color();
                s.R = this.R;
                s.G = this.G;
                s.B = this.B;
                s.A = this.A;
                s.ColorNumber = this.ColorNumber;
                return s;
            }
        }
    });

    Bridge.define("Bidons.Base.Font", {
        fields: {
            Path: null,
            Size: 0
        },
        ctors: {
            ctor: function (path, size) {
                this.$initialize();
                /* 
                			[[
                				this.Font = love.graphics.newFont(path, size)
                			]] 
                			*/
            }
        }
    });

    Bridge.define("Bidons.Base.Image");

    Bridge.define("Bidons.Base.Location", {
        $kind: "struct",
        statics: {
            fields: {
                Zero: null
            },
            ctors: {
                init: function () {
                    this.Zero = new Bidons.Base.Location();
                    this.Zero = new Bidons.Base.Location.$ctor1(0, 0);
                }
            },
            methods: {
                op_Implicit$2: function (tuple) {
                    return new Bidons.Base.Location.$ctor1(tuple.Item1, tuple.Item2);
                },
                op_Implicit$3: function (tuple) {
                    return new Bidons.Base.Location.$ctor1(tuple.Item1, tuple.Item2);
                },
                op_Implicit$1: function (array) {
                    if (array.length !== 2) {
                        throw new System.InvalidCastException.$ctor1("Cannot convert int[] to Location, wrong array length");
                    }
                    return new Bidons.Base.Location.$ctor1(array[System.Array.index(0, array)], array[System.Array.index(1, array)]);
                },
                op_Implicit: function (array) {
                    if (array.length !== 2) {
                        throw new System.InvalidCastException.$ctor1("Cannot convert double[] to Location, wrong array length");
                    }
                    return new Bidons.Base.Location.$ctor1(array[System.Array.index(0, array)], array[System.Array.index(1, array)]);
                },
                getDefaultValue: function () { return new Bidons.Base.Location(); }
            }
        },
        fields: {
            X: 0,
            Y: 0
        },
        ctors: {
            $ctor1: function (x, y) {
                this.$initialize();
                this.X = x;
                this.Y = y;
            },
            ctor: function () {
                this.$initialize();
            }
        },
        methods: {
            Offset$1: function (offsetX, offsetY) {
                return new Bidons.Base.Location.$ctor1(this.X + offsetX, this.Y + offsetY);
            },
            Offset: function (offset) {
                return new Bidons.Base.Location.$ctor1(this.X + offset.X, this.Y + offset.Y);
            },
            NegativeOffset$1: function (offsetX, offsetY) {
                return new Bidons.Base.Location.$ctor1(this.X - offsetX, this.Y - offsetY);
            },
            NegativeOffset: function (offset) {
                return new Bidons.Base.Location.$ctor1(this.X - offset.X, this.Y - offset.Y);
            },
            getHashCode: function () {
                var h = Bridge.addHash([3486701760, this.X, this.Y]);
                return h;
            },
            equals: function (o) {
                if (!Bridge.is(o, Bidons.Base.Location)) {
                    return false;
                }
                return Bridge.equals(this.X, o.X) && Bridge.equals(this.Y, o.Y);
            },
            $clone: function (to) {
                var s = to || new Bidons.Base.Location();
                s.X = this.X;
                s.Y = this.Y;
                return s;
            }
        }
    });

    Bridge.define("Bidons.Base.Shape", {
        $kind: "struct",
        statics: {
            methods: {
                getDefaultValue: function () { return new Bidons.Base.Shape(); }
            }
        },
        methods: {
            $clone: function (to) { return this; }
        }
    });

    Bridge.define("Bidons.Base.Size", {
        $kind: "struct",
        statics: {
            methods: {
                op_Implicit$2: function (tuple) {
                    return new Bidons.Base.Size.$ctor1(tuple.Item1, tuple.Item2);
                },
                op_Implicit$3: function (tuple) {
                    return new Bidons.Base.Size.$ctor1(tuple.Item1, tuple.Item2);
                },
                op_Implicit$1: function (array) {
                    if (array.length !== 2) {
                        throw new System.InvalidCastException.$ctor1("Cannot convert int[] to Size, wrong array length");
                    }
                    return new Bidons.Base.Size.$ctor1(array[System.Array.index(0, array)], array[System.Array.index(1, array)]);
                },
                op_Implicit: function (array) {
                    if (array.length !== 2) {
                        throw new System.InvalidCastException.$ctor1("Cannot convert double[] to Size, wrong array length");
                    }
                    return new Bidons.Base.Size.$ctor1(array[System.Array.index(0, array)], array[System.Array.index(1, array)]);
                },
                getDefaultValue: function () { return new Bidons.Base.Size(); }
            }
        },
        fields: {
            Width: 0,
            Height: 0
        },
        ctors: {
            $ctor1: function (width, height) {
                this.$initialize();
                this.Width = width;
                this.Height = height;
            },
            ctor: function () {
                this.$initialize();
            }
        },
        methods: {
            getHashCode: function () {
                var h = Bridge.addHash([1702521171, this.Width, this.Height]);
                return h;
            },
            equals: function (o) {
                if (!Bridge.is(o, Bidons.Base.Size)) {
                    return false;
                }
                return Bridge.equals(this.Width, o.Width) && Bridge.equals(this.Height, o.Height);
            },
            $clone: function (to) {
                var s = to || new Bidons.Base.Size();
                s.Width = this.Width;
                s.Height = this.Height;
                return s;
            }
        }
    });

    Bridge.define("Bidons.Base.Text");

    Bridge.define("Bidons.Base.Text.Align", {
        $kind: "nested enum",
        statics: {
            fields: {
                Left: 0,
                Center: 1,
                Right: 2
            }
        }
    });

    Bridge.define("Bidons.Core.Compatibility", {
        statics: {
            methods: {
                EntrustOldBscn: function (konstabel, scenePath) {
                    var scene = new Bidons.Core.Scene();
                    konstabel.Entrust(scene);
                }
            }
        }
    });

    Bridge.define("Bidons.IDrawable", {
        $kind: "interface"
    });

    Bridge.define("Bidons.Elements.Element", {
        fields: {
            Scene: null,
            Owner: null
        },
        methods: {
            Create: function () { }
        }
    });

    Bridge.define("Bidons.Core.Context");

    Bridge.define("Bidons.Core.Engine", {
        fields: {
            Konstabel: null,
            TransManager: null,
            SaveLoader: null,
            StackLog: null,
            TopRender: null,
            FastDraw: null,
            Context: null,
            GameId: null,
            GameVersion: null,
            CurrentRenderer: 0
        },
        ctors: {
            init: function () {
                this.CurrentRenderer = Bidons.Core.Engine.Renderer.Electron;
            },
            ctor: function (gameId, gameVersion, context) {
                this.$initialize();
                this.GameId = gameId;
                this.GameVersion = gameVersion;
                this.Konstabel = new Bidons.Core.Konstabel(this);
                this.TransManager = new Bidons.Core.TransManager(this);
                this.SaveLoader = new Bidons.Core.SaveLoader(this);
                this.StackLog = new Bidons.Core.StackLog(this);
                this.TopRender = new Bidons.Core.TopRender(this);
                this.FastDraw = new Bidons.Core.FastDraw(this);
                this.Context = context;

            }
        }
    });

    Bridge.define("Bidons.Core.Engine.Renderer", {
        $kind: "nested enum",
        statics: {
            fields: {
                Electron: 0,
                Love2D: 1
            }
        }
    });

    Bridge.define("Bidons.Core.FastDraw", {
        fields: {
            Engine: null
        },
        ctors: {
            ctor: function (engine) {
                this.$initialize();
                this.Engine = engine;
            }
        },
        methods: {
            DrawImage: function (image) {
                System.Console.WriteLine("hello, word");
            },
            DrawText: function (text) {

            }
        }
    });

    Bridge.define("Bidons.IUpdatable", {
        $kind: "interface"
    });

    Bridge.define("Bidons.Core.Konstabel", {
        fields: {
            Engine: null
        },
        ctors: {
            ctor: function (engine) {
                this.$initialize();
                this.Engine = engine;
            }
        },
        methods: {
            Entrust: function (scene) {
                scene.Engine = this.Engine;
                this.Engine.TopRender.Scene = scene;
                scene.Init();
            },
            Entrust$1: function (sceneName) {

            }
        }
    });

    Bridge.define("Bidons.Events.IMouseEvents", {
        $kind: "interface"
    });

    Bridge.define("Bidons.Events.IKeyEvents", {
        $kind: "interface"
    });

    Bridge.define("Bidons.Events.IEngineEvents", {
        $kind: "interface"
    });

    Bridge.define("Bidons.Events.IContextEvents", {
        $kind: "interface"
    });

    Bridge.define("Bidons.Core.RemoteLog", {
        fields: {
            Engine: null
        },
        ctors: {
            ctor: function (engine) {
                this.$initialize();
                this.Engine = engine;
            }
        }
    });

    Bridge.define("Bidons.Core.SaveLoader", {
        fields: {
            Engine: null
        },
        ctors: {
            ctor: function (engine) {
                this.$initialize();
                this.Engine = engine;
            }
        }
    });

    Bridge.define("Bidons.Core.StackLog", {
        fields: {
            Engine: null
        },
        ctors: {
            ctor: function (engine) {
                this.$initialize();
                this.Engine = engine;
            }
        }
    });

    Bridge.define("Bidons.Core.TransManager", {
        fields: {
            Engine: null
        },
        ctors: {
            ctor: function (engine) {
                this.$initialize();
                this.Engine = engine;
            }
        }
    });

    Bridge.define("Bidons_B.App", {
        main: function Main () {
            var alpha = new Bidons_B.App.Alpha();

            System.Console.WriteLine(System.String.format("Welcome to Bridge.NET {0}!", [alpha.x]));
        }
    });

    Bridge.define("Bidons_B.App.Alpha", {
        $kind: "nested class",
        fields: {
            x: 0,
            y: 0
        },
        ctors: {
            init: function () {
                this.x = 5;
                this.y = 7;
            }
        }
    });

    Bridge.define("Bidons.IContainer", {
        inherits: [Bidons.IDrawable],
        $kind: "interface"
    });

    Bridge.define("Bidons.Elements.Visible", {
        inherits: [Bidons.Elements.Element,Bidons.IDrawable],
        fields: {
            Visibility: false,
            Location: null,
            Anchor: null,
            Size: null
        },
        props: {
            Absolute: {
                get: function () {
                    return this.Origin.NegativeOffset$1(this.Anchor.X * this.Size.Width, this.Anchor.Y * this.Size.Height);
                },
                set: function (value) { }
            },
            Origin: {
                get: function () {
                    if (this.Owner != null) {
                        return this.Location.Offset(Bridge.cast(this.Owner, Bidons.Elements.Visible).Absolute.$clone());
                    } else {
                        return Bidons.Base.Location.Zero.$clone();
                    }
                },
                set: function (value) { }
            }
        },
        ctors: {
            init: function () {
                this.Location = new Bidons.Base.Location();
                this.Anchor = new Bidons.Base.Anchor();
                this.Size = new Bidons.Base.Size();
            }
        }
    });

    Bridge.define("Bidons.Core.Flux", {
        inherits: [Bidons.IUpdatable],
        alias: ["Update", "Bidons$IUpdatable$Update"],
        methods: {
            Update: function (dt) { }
        }
    });

    Bridge.define("Bidons.Events.IEventable", {
        inherits: [Bidons.Events.IContextEvents,Bidons.Events.IEngineEvents,Bidons.Events.IKeyEvents,Bidons.Events.IMouseEvents],
        $kind: "interface"
    });

    Bridge.define("Bidons.Core.Timer", {
        inherits: [Bidons.IUpdatable],
        alias: ["Update", "Bidons$IUpdatable$Update"],
        methods: {
            Update: function (dt) { }
        }
    });

    Bridge.define("Bidons.Elements.Console", {
        inherits: [Bidons.Elements.Element]
    });

    Bridge.define("Bidons.Elements.Dialog", {
        inherits: [Bidons.Elements.Element]
    });

    Bridge.define("Bidons.ElementsList", {
        inherits: [System.Collections.Generic.List$1(Bidons.Elements.Element)],
        fields: {
            Owner: null,
            Scene: null
        },
        methods: {
            add$1: function (item) {
                item.Scene = this.Scene;
                item.Owner = this.Owner;
                this.add(item);
            }
        }
    });

    Bridge.define("Bidons.Elements.Box", {
        inherits: [Bidons.Elements.Visible,Bidons.IContainer],
        fields: {
            Elements: null
        },
        alias: [
            "Draw", "Bidons$IDrawable$Draw",
            "DrawAt", "Bidons$IDrawable$DrawAt"
        ],
        ctors: {
            ctor: function () {
                var $t;
                this.$initialize();
                Bidons.Elements.Visible.ctor.call(this);
                this.Elements = ($t = new Bidons.ElementsList(), $t.Owner = this, $t.Scene = this.Scene, $t);
            }
        },
        methods: {
            Draw: function () {
                throw new System.NotImplementedException.ctor();
            },
            DrawAt: function (x, y) {
                throw new System.NotImplementedException.ctor();
            },
            Bidons$IContainer$Elements: function () {
                return this.Elements;
            }
        }
    });

    Bridge.define("Bidons.Core.Scene", {
        inherits: [Bidons.IContainer,Bidons.Events.IEventable],
        fields: {
            Engine: null,
            Elements: null
        },
        alias: [
            "Draw", "Bidons$IDrawable$Draw",
            "DrawAt", "Bidons$IDrawable$DrawAt"
        ],
        ctors: {
            init: function () {
                this.Elements = new Bidons.ElementsList();
            },
            ctor: function () {
                var $t;
                this.$initialize();
                this.Elements = ($t = new Bidons.ElementsList(), $t.Owner = this, $t.Scene = this, $t);
            }
        },
        methods: {
            Bidons$IContainer$Elements: function () {
                return this.Elements;
            },
            Draw: function () {
                var $t;
                $t = Bridge.getEnumerator(System.Linq.Enumerable.from(this.Elements).where(function (w) {
                        return Bridge.is(w, Bidons.Elements.Visible);
                    }));
                try {
                    while ($t.moveNext()) {
                        var item = Bridge.cast($t.Current, Bidons.Elements.Visible);
                        item.Draw();
                    }
                } finally {
                    if (Bridge.is($t, System.IDisposable)) {
                        $t.System$IDisposable$Dispose();
                    }
                }
            },
            DrawAt: function (x, y) {
                this.Draw();
            },
            KeyPressed: function () {
                throw new System.NotImplementedException.ctor();
            },
            Update: function (dt) {
                throw new System.NotImplementedException.ctor();
            },
            Init: function () {
                var $t;
                $t = Bridge.getEnumerator(this.Elements);
                try {
                    while ($t.moveNext()) {
                        var item = $t.Current;
                        item.Create();
                    }
                } finally {
                    if (Bridge.is($t, System.IDisposable)) {
                        $t.System$IDisposable$Dispose();
                    }
                }

            }
        }
    });

    Bridge.define("Bidons.Core.Overlay", {
        inherits: [Bidons.IContainer,Bidons.Events.IEventable],
        fields: {
            Scene: null,
            Elements: null
        },
        alias: [
            "Draw", "Bidons$IDrawable$Draw",
            "DrawAt", "Bidons$IDrawable$DrawAt"
        ],
        ctors: {
            ctor: function (scene) {
                var $t;
                this.$initialize();
                this.Scene = scene;
                this.Elements = ($t = new Bidons.ElementsList(), $t.Owner = this, $t.Scene = scene, $t);
            }
        },
        methods: {
            Bidons$IContainer$Elements: function () {
                return this.Elements;
            },
            Draw: function () { },
            DrawAt: function (x, y) {
                this.Draw();
            },
            KeyPressed: function () {
                throw new System.NotImplementedException.ctor();
            },
            Update: function (dt) {
                throw new System.NotImplementedException.ctor();
            }
        }
    });

    Bridge.define("Bidons.Core.TopRender", {
        inherits: [Bidons.IDrawable,Bidons.IUpdatable,Bidons.Events.IEventable],
        fields: {
            Timer: null,
            Flux: null,
            Engine: null,
            Scene: null,
            Overlay: null
        },
        alias: [
            "Draw", "Bidons$IDrawable$Draw",
            "DrawAt", "Bidons$IDrawable$DrawAt",
            "Update", "Bidons$IUpdatable$Update"
        ],
        ctors: {
            init: function () {
                this.Timer = new Bidons.Core.Timer();
                this.Flux = new Bidons.Core.Flux();
            },
            ctor: function (engine) {
                this.$initialize();
                this.Engine = engine;

                /* 
                			[[
                				love.draw = function()
                					this:Draw()
                				end

                				love.update = function(dt)
                					this:Update(dt)
                				end
                			]] 
                			*/

            }
        },
        methods: {
            Draw: function () {
                this.Scene != null ? this.Scene.Draw() : null;
                this.Overlay != null ? this.Overlay.Draw() : null;
            },
            DrawAt: function (x, y) {
                this.Draw();
            },
            Update: function (dt) {
                this.Timer != null ? this.Timer.Update(dt) : null;
                this.Flux != null ? this.Flux.Update(dt) : null;
            }
        }
    });

    Bridge.define("Bidons.Elements.Circle", {
        inherits: [Bidons.Elements.Visible],
        fields: {
            Radius: 0,
            Color: null
        },
        alias: [
            "Draw", "Bidons$IDrawable$Draw",
            "DrawAt", "Bidons$IDrawable$DrawAt"
        ],
        ctors: {
            init: function () {
                this.Color = new Bidons.Base.Color();
                this.Color = Bidons.Base.Color.White.$clone();
            }
        },
        methods: {
            Create: function () { },
            Draw: function () {
                /* 
                			[[
                				love.graphics.setColor(this.Color.R, this.Color.G, this.Color.B, this.Color.A)
                				love.graphics.circle('fill', this.Location.X, this.Location.Y, this.Radius)
                			]] 
                			*/
            },
            DrawAt: function (x, y) {
                throw new System.NotImplementedException.ctor();
            }
        }
    });

    Bridge.define("Bidons.Elements.Label", {
        inherits: [Bidons.Elements.Visible],
        fields: {
            Text: null,
            Font: null,
            Color: null
        },
        alias: [
            "Draw", "Bidons$IDrawable$Draw",
            "DrawAt", "Bidons$IDrawable$DrawAt"
        ],
        ctors: {
            init: function () {
                this.Color = new Bidons.Base.Color();
                this.Font = new Bidons.Base.Font("Pixel.ttf", 16);
                this.Color = Bidons.Base.Color.White.$clone();
            }
        },
        methods: {
            Create: function () { },
            Draw: function () {
                /* 
                			[[
                   love.graphics.setColor(this.Color.R, this.Color.G, this.Color.B, this.Color.A)
                				love.graphics.setFont(this.Font.Font)
                				love.graphics.print(this.Text, this.Origin.X, this.Origin.Y);
                			]] 
                			*/
            },
            DrawAt: function (x, y) {
                throw new System.NotImplementedException.ctor();
            }
        }
    });

    Bridge.define("Bidons.Elements.Progress", {
        inherits: [Bidons.Elements.Visible],
        alias: [
            "Draw", "Bidons$IDrawable$Draw",
            "DrawAt", "Bidons$IDrawable$DrawAt"
        ],
        methods: {
            Draw: function () {
                throw new System.NotImplementedException.ctor();
            },
            DrawAt: function (x, y) {
                throw new System.NotImplementedException.ctor();
            }
        }
    });

    Bridge.define("Bidons.Elements.Text", {
        inherits: [Bidons.Elements.Visible],
        alias: [
            "Draw", "Bidons$IDrawable$Draw",
            "DrawAt", "Bidons$IDrawable$DrawAt"
        ],
        methods: {
            Draw: function () {
                throw new System.NotImplementedException.ctor();
            },
            DrawAt: function (x, y) {
                throw new System.NotImplementedException.ctor();
            }
        }
    });

    Bridge.define("Bidons.Core.Compatibility.Box", {
        inherits: [Bidons.Elements.Box],
        $kind: "nested class",
        fields: {
            _color: null
        },
        props: {
            BackColor: {
                get: function () {
                    return this._color.$clone();
                },
                set: function (value) {
                    this._color = value.$clone();
                }
            }
        },
        alias: ["Draw", "Bidons$IDrawable$Draw"],
        ctors: {
            init: function () {
                this._color = new Bidons.Base.Color();
                this._color = Bidons.Base.Color.White.$clone();
            },
            $ctor1: function (source) {
                this.$initialize();
                Bidons.Elements.Box.ctor.call(this);
                if (source.Color) {
                    this._color = source.Color;
                }
                if (source["@" + source.Color]) {

                }

            },
            ctor: function () {
                this.$initialize();
                Bidons.Elements.Box.ctor.call(this);
            }
        },
        methods: {
            Draw: function () {

            },
            Create: function () { }
        }
    });

    Bridge.define("Bidons.Core.OldScene", {
        inherits: [Bidons.Core.Scene],
        fields: {
            Source: null
        },
        ctors: {
            ctor: function () {
                this.$initialize();
                Bidons.Core.Scene.ctor.call(this);
            },
            $ctor1: function (scenePath) {
                this.$initialize();
                Bidons.Core.Scene.ctor.call(this);
            }
        }
    });

    Bridge.define("Bidons.Elements.Array", {
        inherits: [Bidons.Elements.Box]
    });

    Bridge.define("Bidons.Elements.Grid", {
        inherits: [Bidons.Elements.Box]
    });

    Bridge.define("MyGame.SimpleScene", {
        inherits: [Bidons.Core.Scene],
        ctors: {
            ctor: function () {
                var $t;
                this.$initialize();
                Bidons.Core.Scene.ctor.call(this);
                this.Elements.add$1(($t = new Bidons.Elements.Circle(), $t.Location = Bidons.Base.Location.op_Implicit$1(System.Array.init([0, 0], System.Int32)), $t.Radius = 25, $t.Color = Bidons.Base.Color.Blue.$clone(), $t));
                this.Elements.add$1(($t = new Bidons.Elements.Circle(), $t.Location = Bidons.Base.Location.op_Implicit$1(System.Array.init([100, 150], System.Int32)), $t.Radius = 35, $t.Color = Bidons.Base.Color.Red.$clone(), $t));
                this.Elements.add$1(($t = new Bidons.Elements.Label(), $t.Text = "Game Over!", $t.Color = Bidons.Base.Color.Red.$clone(), $t));
            }
        }
    });
});

//# sourceMappingURL=data:application/json;base64,ewogICJ2ZXJzaW9uIjogMywKICAiZmlsZSI6ICJCaWRvbnMuanMiLAogICJzb3VyY2VSb290IjogIiIsCiAgInNvdXJjZXMiOiBbIkJhc2UvQW5jaG9yLmNzIiwiQmFzZS9Db2xvci5jcyIsIkJhc2UvRm9udC5jcyIsIkJhc2UvTG9jYXRpb24uY3MiLCJCYXNlL1NpemUuY3MiLCJDb3JlL0NvbXBhdGliaWxpdHkuY3MiLCJDb3JlL0VuZ2luZS5jcyIsIkNvcmUvRmFzdERyYXcuY3MiLCJDb3JlL0tvbnN0YWJlbC5jcyIsIkNvcmUvUmVtb3RlTG9nLmNzIiwiQ29yZS9TYXZlTG9hZGVyLmNzIiwiQ29yZS9TdGFja0xvZy5jcyIsIkNvcmUvVHJhbnNNYW5hZ2VyLmNzIiwiQXBwLmNzIiwiRWxlbWVudHMvVmlzaWJsZS5jcyIsIkNvcmUvRmx1eC5jcyIsIkNvcmUvVGltZXIuY3MiLCJJQ29udGFpbmVyLmNzIiwiRWxlbWVudHMvQm94LmNzIiwiQ29yZS9TY2VuZS5jcyIsIkNvcmUvT3ZlcmxheS5jcyIsIkNvcmUvVG9wUmVuZGVyLmNzIiwiRWxlbWVudHMvQ2lyY2xlLmNzIiwiRWxlbWVudHMvTGFiZWwuY3MiLCJFbGVtZW50cy9Qcm9ncmVzcy5jcyIsIkVsZW1lbnRzL1RleHQuY3MiLCJDb3JlL09sZFNjZW5lLmNzIiwiU2ltcGxlU2NlbmUuY3MiXSwKICAibmFtZXMiOiBbIiJdLAogICJtYXBwaW5ncyI6ICI7Ozs7Ozs7Ozs7Ozt5Q0FldUNBO29CQUVuQ0EsT0FBT0EsSUFBSUEsMEJBQU9BLGFBQWFBOzt5Q0FDS0E7b0JBRXBDQSxPQUFPQSxJQUFJQSwwQkFBT0EsYUFBYUE7O3lDQUVNQTtvQkFFdENBLElBQUlBO3dCQUFtQkEsTUFBTUEsSUFBSUE7O29CQUNqQ0EsT0FBT0EsSUFBSUEsMEJBQU9BLHFDQUFVQTs7dUNBR1VBO29CQUV0Q0EsSUFBSUE7d0JBQW1CQSxNQUFNQSxJQUFJQTs7b0JBQ2pDQSxPQUFPQSxJQUFJQSwwQkFBT0EscUNBQVVBOzs7Ozs7Ozs7OzhCQXJCZkEsR0FBVUE7O2dCQUV2QkEsU0FBSUE7Z0JBQ0pBLFNBQUlBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OytCQ3dCcUJBLElBQUlBO2lDQUNGQSxJQUFJQTtnQ0FDTEEsSUFBSUE7aUNBQ0hBLElBQUlBO2lDQUNKQSxJQUFJQTs7Ozt1Q0FSSUE7b0JBRWxDQSxPQUFPQSxJQUFJQSx5QkFBTUE7Ozs7Ozs7Ozs7Ozs7OEJBbkJOQTtvREFBeUJBLDhCQUFnQkE7OzhCQUl6Q0E7O2dCQUVaQSxtQkFBY0E7Z0JBQ2RBLGFBQWdCQSwrQkFBc0JBO2dCQUN0Q0EsSUFBSUEsQ0FBQ0E7b0JBQTZCQSxxQkFBY0E7O2dCQUNoREEsU0FBSUE7Z0JBQ0pBLFNBQUlBO2dCQUNKQSxTQUFJQTtnQkFDSkE7Z0JBQ0FBO2dCQUNBQTtnQkFDQUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OzRCQ25CV0EsTUFBYUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Z0NDSldBLElBQUlBOzs7O3lDQTRCREE7b0JBRXJDQSxPQUFPQSxJQUFJQSw0QkFBU0EsYUFBYUE7O3lDQUNLQTtvQkFFdENBLE9BQU9BLElBQUlBLDRCQUFTQSxhQUFhQTs7eUNBRU1BO29CQUV4Q0EsSUFBSUE7d0JBQW1CQSxNQUFNQSxJQUFJQTs7b0JBQ2pDQSxPQUFPQSxJQUFJQSw0QkFBU0EscUNBQVVBOzt1Q0FHVUE7b0JBRXhDQSxJQUFJQTt3QkFBbUJBLE1BQU1BLElBQUlBOztvQkFDakNBLE9BQU9BLElBQUlBLDRCQUFTQSxxQ0FBVUE7Ozs7Ozs7Ozs7OEJBeENmQSxHQUFVQTs7Z0JBQ3pCQSxTQUFJQTtnQkFDSkEsU0FBSUE7Ozs7Ozs7Z0NBR3dCQSxTQUFnQkE7Z0JBRW5DQSxPQUFPQSxJQUFJQSw0QkFBU0EsU0FBSUEsU0FBU0EsU0FBSUE7OzhCQUdsQkE7Z0JBRW5CQSxPQUFPQSxJQUFJQSw0QkFBU0EsU0FBSUEsVUFBVUEsU0FBSUE7O3dDQUdYQSxTQUFnQkE7Z0JBRTNDQSxPQUFPQSxJQUFJQSw0QkFBU0EsU0FBSUEsU0FBU0EsU0FBSUE7O3NDQUdWQTtnQkFFM0JBLE9BQU9BLElBQUlBLDRCQUFTQSxTQUFJQSxVQUFVQSxTQUFJQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozt5Q0NsQmJBO29CQUVqQ0EsT0FBT0EsSUFBSUEsd0JBQUtBLGFBQWFBOzt5Q0FDS0E7b0JBRWxDQSxPQUFPQSxJQUFJQSx3QkFBS0EsYUFBYUE7O3lDQUVNQTtvQkFFcENBLElBQUlBO3dCQUFtQkEsTUFBTUEsSUFBSUE7O29CQUNqQ0EsT0FBT0EsSUFBSUEsd0JBQUtBLHFDQUFVQTs7dUNBR1VBO29CQUVwQ0EsSUFBSUE7d0JBQW1CQSxNQUFNQSxJQUFJQTs7b0JBQ2pDQSxPQUFPQSxJQUFJQSx3QkFBS0EscUNBQVVBOzs7Ozs7Ozs7OzhCQXJCZkEsT0FBY0E7O2dCQUV6QkEsYUFBUUE7Z0JBQ1JBLGNBQVNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7MENDRHdCQSxXQUEwQkE7b0JBRTNEQSxZQUFjQSxJQUFJQTtvQkFDbEJBLGtCQUFrQkE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7dUNDVWVBOzs0QkFPcEJBLFFBQWVBLGFBQW9CQTs7Z0JBRWhEQSxjQUFTQTtnQkFDVEEsbUJBQWNBO2dCQUNkQSxpQkFBWUEsSUFBSUEsc0JBQVVBO2dCQUMxQkEsb0JBQWVBLElBQUlBLHlCQUFhQTtnQkFDaENBLGtCQUFhQSxJQUFJQSx1QkFBV0E7Z0JBQzVCQSxnQkFBV0EsSUFBSUEscUJBQVNBO2dCQUN4QkEsaUJBQVlBLElBQUlBLHNCQUFVQTtnQkFDMUJBLGdCQUFXQSxJQUFJQSxxQkFBU0E7Z0JBQ3hCQSxlQUFVQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OzRCQ2pDS0E7O2dCQUVmQSxjQUFTQTs7OztpQ0FHWUE7Z0JBTXJCQTs7Z0NBSW9CQTs7Ozs7Ozs7Ozs7Ozs7OzRCQ2hCSkE7O2dCQUVoQkEsY0FBU0E7Ozs7K0JBR1VBO2dCQUVuQkEsZUFBZUE7Z0JBQ2ZBLDhCQUF5QkE7Z0JBQ3pCQTs7aUNBR21CQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OzRCQ1pIQTs7Z0JBRWhCQSxjQUFTQTs7Ozs7Ozs7Ozs0QkNGUUE7O2dCQUVqQkEsY0FBU0E7Ozs7Ozs7Ozs7NEJDRk1BOztnQkFFZkEsY0FBU0E7Ozs7Ozs7Ozs7NEJDRlVBOztnQkFFbkJBLGNBQVNBOzs7Ozs7O1lDR1RBLFlBQWdCQSxJQUFJQTs7WUFFcEJBLHlCQUFrQkEsb0RBQTJDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztvQkNIdkJBLE9BQU9BLDZCQUFzQkEsZ0JBQVdBLGlCQUFZQSxnQkFBV0E7Ozs7OztvQkFNeEZBLElBQUlBLGNBQVNBO3dCQUVUQSxPQUFPQSxxQkFBZ0JBLEFBQUNBLFlBQVVBOzt3QkFJbENBLE9BQU9BOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OzhCQ2pCTkE7Ozs7Ozs7Ozs7Ozs7OEJDQUFBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OzZCQ1VDQTtnQkFFbkJBLGFBQWFBO2dCQUNiQSxhQUFhQTtnQkFDYkEsU0FBU0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Z0JDVlRBLGdCQUFXQSxVQUFJQSxrQ0FBdUJBLGlCQUFjQTs7Ozs7Z0JBS3BEQSxNQUFNQSxJQUFJQTs7OEJBR2lCQSxHQUFVQTtnQkFFckNBLE1BQU1BLElBQUlBOzs7Z0JBS1ZBLE9BQU9BOzs7Ozs7Ozs7Ozs7Ozs7OztnQ0NSdUJBLElBQUlBOzs7OztnQkFJbENBLGdCQUFXQSxVQUFJQSxrQ0FBdUJBLGlCQUFjQTs7Ozs7Z0JBUHBEQSxPQUFPQTs7OztnQkFhRUEsMEJBQXlCQSw0QkFBc0NBLHFCQUFTQSxBQUFxQkE7K0JBQUtBOzs7Ozt3QkFFMUdBOzs7Ozs7Ozs4QkFJaUJBLEdBQVVBO2dCQUU1QkE7OztnQkFLQUEsTUFBTUEsSUFBSUE7OzhCQUdRQTtnQkFFbEJBLE1BQU1BLElBQUlBOzs7O2dCQUtWQSwwQkFBcUJBOzs7O3dCQUVwQkE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OzRCQ3pDYUE7OztnQkFFZEEsYUFBUUE7Z0JBQ1JBLGdCQUFXQSxVQUFJQSxrQ0FBdUJBLGlCQUFjQTs7Ozs7Z0JBS3BEQSxPQUFPQTs7OzhCQU1XQSxHQUFVQTtnQkFFNUJBOzs7Z0JBS0FBLE1BQU1BLElBQUlBOzs4QkFHUUE7Z0JBRWxCQSxNQUFNQSxJQUFJQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OzZCQzlCVUEsSUFBSUE7NEJBQ05BLElBQUlBOzs0QkFHTkE7O2dCQUVoQkEsY0FBU0E7Ozs7Ozs7Ozs7Ozs7Ozs7OztnQkF3QlRBLGNBQU9BLE9BQUtBLEFBQXFDQSxvQkFBY0E7Z0JBQy9EQSxnQkFBU0EsT0FBS0EsQUFBcUNBLHNCQUFnQkE7OzhCQUlqREEsR0FBVUE7Z0JBRTVCQTs7OEJBR2tCQTtnQkFFbEJBLGNBQU9BLE9BQUtBLEFBQXFDQSxrQkFBYUEsTUFBS0E7Z0JBQ25FQSxhQUFNQSxPQUFLQSxBQUFxQ0EsaUJBQVlBLE1BQUtBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7NkJDUnZCQTs7Ozs7Ozs7Ozs7Ozs4QkFOZkEsR0FBVUE7Z0JBRXJDQSxNQUFNQSxJQUFJQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs0QkNLOEJBLElBQUlBOzZCQUFtRUE7Ozs7Ozs7Ozs7Ozs7OzhCQU45RUEsR0FBVUE7Z0JBRWxDQSxNQUFNQSxJQUFJQTs7Ozs7Ozs7Ozs7OztnQkMvQm5CQSxNQUFNQSxJQUFJQTs7OEJBR2lCQSxHQUFVQTtnQkFFckNBLE1BQU1BLElBQUlBOzs7Ozs7Ozs7Ozs7O2dCQ0xWQSxNQUFNQSxJQUFJQTs7OEJBR2lCQSxHQUFVQTtnQkFFckNBLE1BQU1BLElBQUlBOzs7Ozs7Ozs7Ozs7OztvQnBCU0hBLE9BQU9BOzs7b0JBR1pBLGNBQVNBOzs7Ozs7Ozs4QkFOV0E7OzhCQVVYQTs7O2dCQUVWQSxJQUFJQTtvQkFBY0EsY0FBU0E7O2dCQUMzQkEsSUFBSUEsT0FBT0EsTUFBTUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OEJxQmxCSEE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Z0JDRGZBLG9CQUFhQSxVQUFJQSx3Q0FBb0JBLG1DQUFXQSxxRUFBcUNBO2dCQUM1RUEsb0JBQWFBLFVBQUlBLHdDQUFvQkEsbUNBQVdBLHlFQUF5Q0E7Z0JBQ3pGQSxvQkFBYUEsVUFBSUEsNERBQXFDQSIsCiAgInNvdXJjZXNDb250ZW50IjogWyJ1c2luZyBTeXN0ZW07XHJcbnVzaW5nIFN5c3RlbS5Db2xsZWN0aW9ucy5HZW5lcmljO1xyXG51c2luZyBTeXN0ZW0uVGV4dDtcclxuXHJcbm5hbWVzcGFjZSBCaWRvbnMuQmFzZVxyXG57XHJcblx0cHVibGljIHN0cnVjdCBBbmNob3JcclxuXHR7XHJcblx0XHRwdWJsaWMgZG91YmxlIFg7XHJcblx0XHRwdWJsaWMgZG91YmxlIFk7XHJcblx0XHRwdWJsaWMgQW5jaG9yKGRvdWJsZSB4LCBkb3VibGUgeSlcclxuXHRcdHtcclxuXHRcdFx0WCA9IHg7XHJcblx0XHRcdFkgPSB5O1xyXG5cdFx0fVxyXG5wdWJsaWMgc3RhdGljIGltcGxpY2l0IG9wZXJhdG9yIEFuY2hvcihUdXBsZTxkb3VibGUsIGRvdWJsZT4gdHVwbGUpXHJcbntcclxuICAgIHJldHVybiBuZXcgQW5jaG9yKHR1cGxlLkl0ZW0xLCB0dXBsZS5JdGVtMik7XHJcbn1wdWJsaWMgc3RhdGljIGltcGxpY2l0IG9wZXJhdG9yIEFuY2hvcihUdXBsZTxpbnQsIGludD4gdHVwbGUpXHJcbntcclxuICAgIHJldHVybiBuZXcgQW5jaG9yKHR1cGxlLkl0ZW0xLCB0dXBsZS5JdGVtMik7XHJcbn1cclxuXHRcdHB1YmxpYyBzdGF0aWMgaW1wbGljaXQgb3BlcmF0b3IgQW5jaG9yKGludFtdIGFycmF5KVxyXG5cdFx0e1xyXG5cdFx0XHRpZiAoYXJyYXkuTGVuZ3RoICE9IDIpIHRocm93IG5ldyBJbnZhbGlkQ2FzdEV4Y2VwdGlvbihcIkNhbm5vdCBjb252ZXJ0IGludFtdIHRvIEFuY2hvciwgd3JvbmcgYXJyYXkgbGVuZ3RoXCIpO1xyXG5cdFx0XHRyZXR1cm4gbmV3IEFuY2hvcihhcnJheVswXSwgYXJyYXlbMV0pO1xyXG5cdFx0fVxyXG5cclxuXHRcdHB1YmxpYyBzdGF0aWMgaW1wbGljaXQgb3BlcmF0b3IgQW5jaG9yKGRvdWJsZVtdIGFycmF5KVxyXG5cdFx0e1xyXG5cdFx0XHRpZiAoYXJyYXkuTGVuZ3RoICE9IDIpIHRocm93IG5ldyBJbnZhbGlkQ2FzdEV4Y2VwdGlvbihcIkNhbm5vdCBjb252ZXJ0IGRvdWJsZVtdIHRvIEFuY2hvciwgd3JvbmcgYXJyYXkgbGVuZ3RoXCIpO1xyXG5cdFx0XHRyZXR1cm4gbmV3IEFuY2hvcihhcnJheVswXSwgYXJyYXlbMV0pO1xyXG5cdFx0fVxyXG5cdH1cclxufVxyXG4iLCJ1c2luZyBTeXN0ZW07XHJcbnVzaW5nIFN5c3RlbS5Db2xsZWN0aW9ucy5HZW5lcmljO1xyXG51c2luZyBTeXN0ZW0uTGlucTtcclxudXNpbmcgU3lzdGVtLlRleHQ7XHJcbnVzaW5nIFN5c3RlbS5UaHJlYWRpbmcuVGFza3M7XHJcblxyXG5uYW1lc3BhY2UgQmlkb25zLkJhc2Vcclxue1xyXG5cdHB1YmxpYyBzdHJ1Y3QgQ29sb3JcclxuXHR7XHJcblx0XHRwdWJsaWMgZmxvYXQgUjtcclxuXHRcdHB1YmxpYyBmbG9hdCBHO1xyXG5cdFx0cHVibGljIGZsb2F0IEI7XHJcblx0XHRwdWJsaWMgZmxvYXQgQTtcclxuXHRcdHB1YmxpYyBpbnQgQ29sb3JOdW1iZXI7XHJcblxyXG5cdFx0cHVibGljIENvbG9yKHN0cmluZyBzdHJpbmdIZXgpIDogdGhpcyhDb252ZXJ0LlRvSW50MzIoc3RyaW5nSGV4LlJlcGxhY2UoXCIjXCIsIFwiXCIpLCAxNikpXHJcblx0XHR7XHJcblx0XHR9XHJcblxyXG5cdFx0cHVibGljIENvbG9yKGludCBjb2xvck51bWJlcilcclxuXHRcdHtcclxuXHRcdFx0Q29sb3JOdW1iZXIgPSBjb2xvck51bWJlcjtcclxuXHRcdFx0Ynl0ZVtdIHZhbHVlcyA9IEJpdENvbnZlcnRlci5HZXRCeXRlcyhjb2xvck51bWJlcik7XHJcblx0XHRcdGlmICghQml0Q29udmVydGVyLklzTGl0dGxlRW5kaWFuKSBBcnJheS5SZXZlcnNlKHZhbHVlcyk7XHJcblx0XHRcdFIgPSB2YWx1ZXNbMl07XHJcblx0XHRcdEcgPSB2YWx1ZXNbMV07XHJcblx0XHRcdEIgPSB2YWx1ZXNbMF07XHJcblx0XHRcdFIgLz0gMjU1O1xyXG5cdFx0XHRHIC89IDI1NTtcclxuXHRcdFx0QiAvPSAyNTU7XHJcblx0XHRcdEEgPSAxO1xyXG5cdFx0fVxyXG5wdWJsaWMgc3RhdGljIGltcGxpY2l0IG9wZXJhdG9yIENvbG9yKGludCBjb2xvck51bWJlcilcclxue1xyXG4gICAgcmV0dXJuIG5ldyBDb2xvcihjb2xvck51bWJlcik7XHJcbn1cclxuXHRcdHB1YmxpYyBzdGF0aWMgQ29sb3IgUmVkID0gbmV3IENvbG9yKDB4ZmYwMDAwKTtcclxuXHRcdHB1YmxpYyBzdGF0aWMgQ29sb3IgR3JlZW4gPSBuZXcgQ29sb3IoMHgwMGZmMDApO1xyXG5cdFx0cHVibGljIHN0YXRpYyBDb2xvciBCbHVlID0gbmV3IENvbG9yKDB4MDAwMGZmKTtcclxuXHRcdHB1YmxpYyBzdGF0aWMgQ29sb3IgQmxhY2sgPSBuZXcgQ29sb3IoMHgwMDAwMDApO1xyXG5cdFx0cHVibGljIHN0YXRpYyBDb2xvciBXaGl0ZSA9IG5ldyBDb2xvcigweGZmZmZmZik7XHJcblx0fVxyXG59XHJcbiIsInVzaW5nIFN5c3RlbTtcclxudXNpbmcgU3lzdGVtLkNvbGxlY3Rpb25zLkdlbmVyaWM7XHJcbnVzaW5nIFN5c3RlbS5MaW5xO1xyXG51c2luZyBTeXN0ZW0uVGV4dDtcclxudXNpbmcgU3lzdGVtLlRocmVhZGluZy5UYXNrcztcclxuXHJcbm5hbWVzcGFjZSBCaWRvbnMuQmFzZVxyXG57XHJcblx0cHVibGljIGNsYXNzIEZvbnRcclxuXHR7XHJcblx0XHRwdWJsaWMgc3RyaW5nIFBhdGggeyBnZXQ7IHNldDsgfVxyXG5cdFx0cHVibGljIGZsb2F0IFNpemUgeyBnZXQ7IHNldDsgfVxyXG5cdFx0cHVibGljIEZvbnQoc3RyaW5nIHBhdGgsIGZsb2F0IHNpemUpXHJcblx0XHR7XHJcbiNpZiBMT1ZFXHJcbiAgICAgICAgICAgIC8qXHJcblx0XHRcdFtbXHJcblx0XHRcdFx0dGhpcy5Gb250ID0gbG92ZS5ncmFwaGljcy5uZXdGb250KHBhdGgsIHNpemUpXHJcblx0XHRcdF1dIFxyXG5cdFx0XHQqL1xyXG4jZW5kaWZcclxuI2lmIEVMRUNUUk9cclxuXHJcbiNlbmRpZlxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcblxyXG59XHJcbiIsInVzaW5nIFN5c3RlbTtcclxudXNpbmcgU3lzdGVtLkNvbGxlY3Rpb25zLkdlbmVyaWM7XHJcbnVzaW5nIFN5c3RlbS5UZXh0O1xyXG5cclxubmFtZXNwYWNlIEJpZG9ucy5CYXNlXHJcbntcclxuXHRwdWJsaWMgc3RydWN0IExvY2F0aW9uXHJcblx0e1xyXG4gICAgICAgIHB1YmxpYyBzdGF0aWMgTG9jYXRpb24gWmVybyA9IG5ldyBMb2NhdGlvbigwLCAwKTtcclxuXHJcblx0XHRwdWJsaWMgZG91YmxlIFg7XHJcblx0XHRwdWJsaWMgZG91YmxlIFk7XHJcblx0XHRwdWJsaWMgTG9jYXRpb24oZG91YmxlIHgsIGRvdWJsZSB5KSB7XHJcblx0XHRcdFggPSB4O1xyXG5cdFx0XHRZID0geTtcclxuXHRcdH1cclxuXHJcbiAgICAgICAgcHVibGljIExvY2F0aW9uIE9mZnNldChkb3VibGUgb2Zmc2V0WCwgZG91YmxlIG9mZnNldFkpXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICByZXR1cm4gbmV3IExvY2F0aW9uKFggKyBvZmZzZXRYLCBZICsgb2Zmc2V0WSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBwdWJsaWMgTG9jYXRpb24gT2Zmc2V0KExvY2F0aW9uIG9mZnNldClcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIHJldHVybiBuZXcgTG9jYXRpb24oWCArIG9mZnNldC5YLCBZICsgb2Zmc2V0LlkpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcHVibGljIExvY2F0aW9uIE5lZ2F0aXZlT2Zmc2V0KGRvdWJsZSBvZmZzZXRYLCBkb3VibGUgb2Zmc2V0WSlcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIHJldHVybiBuZXcgTG9jYXRpb24oWCAtIG9mZnNldFgsIFkgLSBvZmZzZXRZKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHB1YmxpYyBMb2NhdGlvbiBOZWdhdGl2ZU9mZnNldChMb2NhdGlvbiBvZmZzZXQpXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICByZXR1cm4gbmV3IExvY2F0aW9uKFggLSBvZmZzZXQuWCwgWSAtIG9mZnNldC5ZKTtcclxuICAgICAgICB9XHJcbnB1YmxpYyBzdGF0aWMgaW1wbGljaXQgb3BlcmF0b3IgTG9jYXRpb24oVHVwbGU8ZG91YmxlLCBkb3VibGU+IHR1cGxlKVxyXG57XHJcbiAgICByZXR1cm4gbmV3IExvY2F0aW9uKHR1cGxlLkl0ZW0xLCB0dXBsZS5JdGVtMik7XHJcbn1wdWJsaWMgc3RhdGljIGltcGxpY2l0IG9wZXJhdG9yIExvY2F0aW9uKFR1cGxlPGludCwgaW50PiB0dXBsZSlcclxue1xyXG4gICAgcmV0dXJuIG5ldyBMb2NhdGlvbih0dXBsZS5JdGVtMSwgdHVwbGUuSXRlbTIpO1xyXG59XHJcblx0XHRwdWJsaWMgc3RhdGljIGltcGxpY2l0IG9wZXJhdG9yIExvY2F0aW9uKGludFtdIGFycmF5KVxyXG5cdFx0e1xyXG5cdFx0XHRpZiAoYXJyYXkuTGVuZ3RoICE9IDIpIHRocm93IG5ldyBJbnZhbGlkQ2FzdEV4Y2VwdGlvbihcIkNhbm5vdCBjb252ZXJ0IGludFtdIHRvIExvY2F0aW9uLCB3cm9uZyBhcnJheSBsZW5ndGhcIik7XHJcblx0XHRcdHJldHVybiBuZXcgTG9jYXRpb24oYXJyYXlbMF0sIGFycmF5WzFdKTtcclxuXHRcdH1cclxuXHJcblx0XHRwdWJsaWMgc3RhdGljIGltcGxpY2l0IG9wZXJhdG9yIExvY2F0aW9uKGRvdWJsZVtdIGFycmF5KVxyXG5cdFx0e1xyXG5cdFx0XHRpZiAoYXJyYXkuTGVuZ3RoICE9IDIpIHRocm93IG5ldyBJbnZhbGlkQ2FzdEV4Y2VwdGlvbihcIkNhbm5vdCBjb252ZXJ0IGRvdWJsZVtdIHRvIExvY2F0aW9uLCB3cm9uZyBhcnJheSBsZW5ndGhcIik7XHJcblx0XHRcdHJldHVybiBuZXcgTG9jYXRpb24oYXJyYXlbMF0sIGFycmF5WzFdKTtcclxuXHRcdH1cclxuXHR9XHJcbn1cclxuIiwidXNpbmcgU3lzdGVtO1xyXG51c2luZyBTeXN0ZW0uQ29sbGVjdGlvbnMuR2VuZXJpYztcclxudXNpbmcgU3lzdGVtLlRleHQ7XHJcblxyXG5uYW1lc3BhY2UgQmlkb25zLkJhc2Vcclxue1xyXG5cdHB1YmxpYyBzdHJ1Y3QgU2l6ZVxyXG5cdHtcclxuXHRcdHB1YmxpYyBkb3VibGUgV2lkdGg7XHJcblx0XHRwdWJsaWMgZG91YmxlIEhlaWdodDtcclxuXHJcblx0XHRwdWJsaWMgU2l6ZShkb3VibGUgd2lkdGgsIGRvdWJsZSBoZWlnaHQpXHJcblx0XHR7XHJcblx0XHRcdFdpZHRoID0gd2lkdGg7XHJcblx0XHRcdEhlaWdodCA9IGhlaWdodDtcclxuXHRcdH1cclxucHVibGljIHN0YXRpYyBpbXBsaWNpdCBvcGVyYXRvciBTaXplKFR1cGxlPGRvdWJsZSwgZG91YmxlPiB0dXBsZSlcclxue1xyXG4gICAgcmV0dXJuIG5ldyBTaXplKHR1cGxlLkl0ZW0xLCB0dXBsZS5JdGVtMik7XHJcbn1wdWJsaWMgc3RhdGljIGltcGxpY2l0IG9wZXJhdG9yIFNpemUoVHVwbGU8aW50LCBpbnQ+IHR1cGxlKVxyXG57XHJcbiAgICByZXR1cm4gbmV3IFNpemUodHVwbGUuSXRlbTEsIHR1cGxlLkl0ZW0yKTtcclxufVxyXG5cdFx0cHVibGljIHN0YXRpYyBpbXBsaWNpdCBvcGVyYXRvciBTaXplKGludFtdIGFycmF5KVxyXG5cdFx0e1xyXG5cdFx0XHRpZiAoYXJyYXkuTGVuZ3RoICE9IDIpIHRocm93IG5ldyBJbnZhbGlkQ2FzdEV4Y2VwdGlvbihcIkNhbm5vdCBjb252ZXJ0IGludFtdIHRvIFNpemUsIHdyb25nIGFycmF5IGxlbmd0aFwiKTtcclxuXHRcdFx0cmV0dXJuIG5ldyBTaXplKGFycmF5WzBdLCBhcnJheVsxXSk7XHJcblx0XHR9XHJcblxyXG5cdFx0cHVibGljIHN0YXRpYyBpbXBsaWNpdCBvcGVyYXRvciBTaXplKGRvdWJsZVtdIGFycmF5KVxyXG5cdFx0e1xyXG5cdFx0XHRpZiAoYXJyYXkuTGVuZ3RoICE9IDIpIHRocm93IG5ldyBJbnZhbGlkQ2FzdEV4Y2VwdGlvbihcIkNhbm5vdCBjb252ZXJ0IGRvdWJsZVtdIHRvIFNpemUsIHdyb25nIGFycmF5IGxlbmd0aFwiKTtcclxuXHRcdFx0cmV0dXJuIG5ldyBTaXplKGFycmF5WzBdLCBhcnJheVsxXSk7XHJcblx0XHR9XHJcblx0fVxyXG59XHJcbiIsInVzaW5nIEJpZG9ucy5CYXNlO1xyXG51c2luZyBCaWRvbnMuRWxlbWVudHM7XHJcbnVzaW5nIFN5c3RlbTtcclxudXNpbmcgU3lzdGVtLkNvbGxlY3Rpb25zLkdlbmVyaWM7XHJcbnVzaW5nIFN5c3RlbS5MaW5xO1xyXG51c2luZyBTeXN0ZW0uVGV4dDtcclxudXNpbmcgU3lzdGVtLlRocmVhZGluZy5UYXNrcztcclxuXHJcbm5hbWVzcGFjZSBCaWRvbnMuQ29yZVxyXG57XHJcblx0W09ic29sZXRlXVxyXG5cdHB1YmxpYyBzdGF0aWMgY2xhc3MgQ29tcGF0aWJpbGl0eVxyXG5cdHtcclxuXHRcdHB1YmxpYyBzdGF0aWMgdm9pZCBFbnRydXN0T2xkQnNjbih0aGlzIEtvbnN0YWJlbCBrb25zdGFiZWwsIHN0cmluZyBzY2VuZVBhdGgpXHJcblx0XHR7XHJcblx0XHRcdFNjZW5lIHNjZW5lID0gbmV3IFNjZW5lKCk7XHJcblx0XHRcdGtvbnN0YWJlbC5FbnRydXN0KHNjZW5lKTtcclxuXHRcdH1cclxuXHJcblx0XHRwdWJsaWMgY2xhc3MgQm94IDogRWxlbWVudHMuQm94XHJcblx0XHR7XHJcblx0XHRcdHB1YmxpYyBDb2xvciBfY29sb3IgPSBDb2xvci5XaGl0ZTtcclxuXHRcdFx0cHVibGljIENvbG9yIEJhY2tDb2xvclxyXG5cdFx0XHR7XHJcblx0XHRcdFx0Z2V0IHsgcmV0dXJuIF9jb2xvcjsgfVxyXG5cdFx0XHRcdHNldFxyXG5cdFx0XHRcdHtcclxuXHRcdFx0XHRcdF9jb2xvciA9IHZhbHVlO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0cHVibGljIEJveChkeW5hbWljIHNvdXJjZSlcclxuICAgICAgICAgICAge1xyXG5cdFx0XHRcdGlmIChzb3VyY2UuQ29sb3IpIF9jb2xvciA9IHNvdXJjZS5Db2xvcjtcclxuXHRcdFx0XHRpZiAoc291cmNlW1wiQFwiICsgc291cmNlLkNvbG9yXSkge1xyXG5cdFx0XHRcdFx0XHJcblx0XHRcdFx0fVxyXG5cdFx0XHRcdFxyXG5cdFx0XHR9XHJcblx0XHRcdHB1YmxpYyBCb3goKSB7IH1cclxuXHJcblx0XHRcdHB1YmxpYyBvdmVycmlkZSB2b2lkIERyYXcoKVxyXG5cdFx0XHR7XHJcblx0XHRcdFx0XHJcblx0XHRcdH1cclxuXHJcblx0XHRcdHB1YmxpYyBvdmVycmlkZSB2b2lkIENyZWF0ZSgpXHJcblx0XHRcdHtcclxuI2lmIEVMRUNUUk9cclxuXHRcdFx0XHRQaXhpT2JqZWN0ID0gU2NlbmUuRW5naW5lLlBJWElIZWxwZXIuY3JlYXRlKFwiQ29udGFpbmVyXCIpO1xyXG5cclxuXHRcdFx0XHRQaXhpT2JqZWN0LnggPSBMb2NhdGlvbi5YO1xyXG5cdFx0XHRcdFBpeGlPYmplY3QueSA9IExvY2F0aW9uLlk7XHJcblx0XHRcdFx0Ly9Pd25lci5QaXguYWRkQ2hpbGQoUGl4aU9iamVjdCk7XHJcbiNlbmRpZlxyXG5cdFx0XHR9XHJcblx0XHR9XHJcblx0fVxyXG59XHJcbiIsIiNpZiBFTEVDVFJPXHJcbnVzaW5nIEJyaWRnZTtcclxudXNpbmcgQnJpZGdlLkh0bWw1O1xyXG4jZW5kaWZcclxuXHJcbnVzaW5nIFN5c3RlbTtcclxuXHJcbm5hbWVzcGFjZSBCaWRvbnMuQ29yZVxyXG57XHJcblx0cHVibGljIGNsYXNzIEVuZ2luZVxyXG5cdHtcclxuXHRcdHB1YmxpYyByZWFkb25seSBLb25zdGFiZWwgS29uc3RhYmVsO1xyXG5cdFx0cHVibGljIHJlYWRvbmx5IFRyYW5zTWFuYWdlciBUcmFuc01hbmFnZXI7XHJcblx0XHRwdWJsaWMgcmVhZG9ubHkgU2F2ZUxvYWRlciBTYXZlTG9hZGVyO1xyXG5cdFx0cHVibGljIHJlYWRvbmx5IFN0YWNrTG9nIFN0YWNrTG9nO1xyXG5cdFx0cHVibGljIHJlYWRvbmx5IFRvcFJlbmRlciBUb3BSZW5kZXI7XHJcblx0XHRwdWJsaWMgcmVhZG9ubHkgRmFzdERyYXcgRmFzdERyYXc7XHJcblx0XHRwdWJsaWMgZHluYW1pYyBDb250ZXh0O1xyXG5cdFx0cHVibGljIHN0cmluZyBHYW1lSWQ7XHJcblx0XHRwdWJsaWMgc3RyaW5nIEdhbWVWZXJzaW9uO1xyXG5cclxuI2lmIEVMRUNUUk9cclxuXHRcdHB1YmxpYyBkeW5hbWljIFBJWElIZWxwZXIgPSBTY3JpcHQuR2V0KFwiUElYSUhlbHBlclwiKTtcclxuXHRcdHB1YmxpYyBkeW5hbWljIFBJWElBcHBsaWNhdGlvbjtcclxuI2VuZGlmXHJcblxyXG5cdFx0cHVibGljIFJlbmRlcmVyIEN1cnJlbnRSZW5kZXJlciA9IFJlbmRlcmVyLkVsZWN0cm9uO1xyXG5cdFx0cHVibGljIGVudW0gUmVuZGVyZXJcclxuXHRcdHtcclxuXHRcdFx0RWxlY3Ryb24sIExvdmUyRFxyXG5cdFx0fVxyXG5cclxuXHJcblx0XHRwdWJsaWMgRW5naW5lKHN0cmluZyBnYW1lSWQsIHN0cmluZyBnYW1lVmVyc2lvbiwgZHluYW1pYyBjb250ZXh0KVxyXG5cdFx0e1xyXG5cdFx0XHRHYW1lSWQgPSBnYW1lSWQ7XHJcblx0XHRcdEdhbWVWZXJzaW9uID0gZ2FtZVZlcnNpb247XHJcblx0XHRcdEtvbnN0YWJlbCA9IG5ldyBLb25zdGFiZWwodGhpcyk7XHJcblx0XHRcdFRyYW5zTWFuYWdlciA9IG5ldyBUcmFuc01hbmFnZXIodGhpcyk7XHJcblx0XHRcdFNhdmVMb2FkZXIgPSBuZXcgU2F2ZUxvYWRlcih0aGlzKTtcclxuXHRcdFx0U3RhY2tMb2cgPSBuZXcgU3RhY2tMb2codGhpcyk7XHJcblx0XHRcdFRvcFJlbmRlciA9IG5ldyBUb3BSZW5kZXIodGhpcyk7XHJcblx0XHRcdEZhc3REcmF3ID0gbmV3IEZhc3REcmF3KHRoaXMpO1xyXG5cdFx0XHRDb250ZXh0ID0gY29udGV4dDtcclxuXHJcbiNpZiBFTEVDVFJPXHJcblx0XHRcdFBJWElBcHBsaWNhdGlvbiA9IFBJWElIZWxwZXIuY3JlYXRlKFwiQXBwbGljYXRpb25cIiwgbmV3IHtcclxuXHRcdFx0XHR3aWR0aCA9IDUwMCxcclxuXHRcdFx0XHRoZWlnaHQgPSA1MDAsXHJcblx0XHRcdFx0dmlldyA9IERvY3VtZW50LkdldEVsZW1lbnRCeUlkKFwibWFpblwiKVxyXG5cdFx0XHR9KTtcclxuI2VuZGlmXHJcblx0XHR9XHJcblx0fVxyXG59XHJcbiIsInVzaW5nIEJpZG9ucy5CYXNlO1xyXG51c2luZyBTeXN0ZW07XHJcbnVzaW5nIFN5c3RlbS5Db2xsZWN0aW9ucy5HZW5lcmljO1xyXG51c2luZyBTeXN0ZW0uVGV4dDtcclxuXHJcbm5hbWVzcGFjZSBCaWRvbnMuQ29yZVxyXG57XHJcblx0cHVibGljIGNsYXNzIEZhc3REcmF3XHJcblx0e1xyXG5cdFx0cHVibGljIHJlYWRvbmx5IEVuZ2luZSBFbmdpbmU7XHJcblx0XHRwdWJsaWMgRmFzdERyYXcoRW5naW5lIGVuZ2luZSlcclxuXHRcdHtcclxuXHRcdFx0RW5naW5lID0gZW5naW5lO1xyXG5cdFx0fVxyXG5cclxuXHRcdHB1YmxpYyB2b2lkIERyYXdJbWFnZShJbWFnZSBpbWFnZSkge1xyXG5cdFx0XHQjaWYgX19DU2hhcnBMdWFfX1xyXG5cdFx0XHQvKltbXHJcblx0XHRcdGxvdmUuZ3JhcGhpY3MucHJpbnQoJ2hlbGxvLCB3b3JkJylcclxuXHRcdFx0XV0qLyAgXHJcblx0XHRcdCNlbHNlXHJcblx0XHRcdENvbnNvbGUuV3JpdGVMaW5lKFwiaGVsbG8sIHdvcmRcIik7XHJcblx0XHRcdCNlbmRpZlxyXG5cdFx0fVxyXG5cclxuXHRcdHB1YmxpYyB2b2lkIERyYXdUZXh0KFRleHQgdGV4dClcclxuXHRcdHtcclxuXHJcblx0XHR9XHJcblx0fVxyXG59XHJcbiIsInVzaW5nIFN5c3RlbTtcclxudXNpbmcgU3lzdGVtLkNvbGxlY3Rpb25zLkdlbmVyaWM7XHJcbnVzaW5nIFN5c3RlbS5UZXh0O1xyXG5cclxubmFtZXNwYWNlIEJpZG9ucy5Db3JlXHJcbntcclxuXHRwdWJsaWMgY2xhc3MgS29uc3RhYmVsXHJcblx0e1xyXG5cdFx0cHVibGljIHJlYWRvbmx5IEVuZ2luZSBFbmdpbmU7XHJcblx0XHRwdWJsaWMgS29uc3RhYmVsKEVuZ2luZSBlbmdpbmUpXHJcblx0XHR7XHJcblx0XHRcdEVuZ2luZSA9IGVuZ2luZTtcclxuXHRcdH1cclxuXHJcblx0XHRwdWJsaWMgdm9pZCBFbnRydXN0KFNjZW5lIHNjZW5lKVxyXG5cdFx0e1xyXG5cdFx0XHRzY2VuZS5FbmdpbmUgPSBFbmdpbmU7XHJcblx0XHRcdEVuZ2luZS5Ub3BSZW5kZXIuU2NlbmUgPSBzY2VuZTtcclxuXHRcdFx0c2NlbmUuSW5pdCgpO1xyXG5cdFx0fVxyXG5cclxuXHRcdHB1YmxpYyB2b2lkIEVudHJ1c3Qoc3RyaW5nIHNjZW5lTmFtZSlcclxuXHRcdHtcclxuXHJcblx0XHR9XHJcblx0fVxyXG59XHJcbiIsInVzaW5nIFN5c3RlbTtcclxudXNpbmcgU3lzdGVtLkNvbGxlY3Rpb25zLkdlbmVyaWM7XHJcbnVzaW5nIFN5c3RlbS5UZXh0O1xyXG5cclxubmFtZXNwYWNlIEJpZG9ucy5Db3JlXHJcbntcclxuXHRwdWJsaWMgY2xhc3MgUmVtb3RlTG9nXHJcblx0e1xyXG5cdFx0cHVibGljIHJlYWRvbmx5IEVuZ2luZSBFbmdpbmU7XHJcblx0XHRwdWJsaWMgUmVtb3RlTG9nKEVuZ2luZSBlbmdpbmUpXHJcblx0XHR7XHJcblx0XHRcdEVuZ2luZSA9IGVuZ2luZTtcclxuXHRcdH1cclxuXHR9XHJcbn1cclxuIiwidXNpbmcgU3lzdGVtO1xyXG51c2luZyBTeXN0ZW0uQ29sbGVjdGlvbnMuR2VuZXJpYztcclxudXNpbmcgU3lzdGVtLlRleHQ7XHJcblxyXG5uYW1lc3BhY2UgQmlkb25zLkNvcmVcclxue1xyXG5cdHB1YmxpYyBjbGFzcyBTYXZlTG9hZGVyXHJcblx0e1xyXG5cdFx0cHVibGljIHJlYWRvbmx5IEVuZ2luZSBFbmdpbmU7XHJcblx0XHRwdWJsaWMgU2F2ZUxvYWRlcihFbmdpbmUgZW5naW5lKVxyXG5cdFx0e1xyXG5cdFx0XHRFbmdpbmUgPSBlbmdpbmU7XHJcblx0XHR9XHJcblx0fVxyXG59XHJcbiIsInVzaW5nIFN5c3RlbTtcclxudXNpbmcgU3lzdGVtLkNvbGxlY3Rpb25zLkdlbmVyaWM7XHJcbnVzaW5nIFN5c3RlbS5UZXh0O1xyXG5cclxubmFtZXNwYWNlIEJpZG9ucy5Db3JlXHJcbntcclxuXHRwdWJsaWMgY2xhc3MgU3RhY2tMb2dcclxuXHR7XHJcblx0XHRwdWJsaWMgcmVhZG9ubHkgRW5naW5lIEVuZ2luZTtcclxuXHRcdHB1YmxpYyBTdGFja0xvZyhFbmdpbmUgZW5naW5lKVxyXG5cdFx0e1xyXG5cdFx0XHRFbmdpbmUgPSBlbmdpbmU7XHJcblx0XHR9XHJcblx0fVxyXG59XHJcbiIsInVzaW5nIFN5c3RlbTtcclxudXNpbmcgU3lzdGVtLkNvbGxlY3Rpb25zLkdlbmVyaWM7XHJcbnVzaW5nIFN5c3RlbS5UZXh0O1xyXG5cclxubmFtZXNwYWNlIEJpZG9ucy5Db3JlXHJcbntcclxuXHRwdWJsaWMgY2xhc3MgVHJhbnNNYW5hZ2VyXHJcblx0e1xyXG5cdFx0cHVibGljIHJlYWRvbmx5IEVuZ2luZSBFbmdpbmU7XHJcblx0XHRwdWJsaWMgVHJhbnNNYW5hZ2VyKEVuZ2luZSBlbmdpbmUpXHJcblx0XHR7XHJcblx0XHRcdEVuZ2luZSA9IGVuZ2luZTtcclxuXHRcdH1cclxuXHR9XHJcbn1cclxuIiwidXNpbmcgU3lzdGVtO1xyXG5cclxubmFtZXNwYWNlIEJpZG9uc19CXHJcbntcclxuXHRwdWJsaWMgY2xhc3MgQXBwXHJcblx0e1xyXG5cdFx0cHVibGljIGNsYXNzIEFscGhhXHJcblx0XHR7XHJcblx0XHRcdHB1YmxpYyBpbnQgeCA9IDU7XHJcblx0XHRcdHB1YmxpYyBpbnQgeSA9IDc7XHJcblx0XHR9XHJcblxyXG5cdFx0cHVibGljIHN0YXRpYyB2b2lkIE1haW4oKVxyXG5cdFx0e1xyXG5cdFx0XHRkeW5hbWljIGFscGhhID0gbmV3IEFscGhhKCk7XHJcblxyXG5cdFx0XHRDb25zb2xlLldyaXRlTGluZShzdHJpbmcuRm9ybWF0KFwiV2VsY29tZSB0byBCcmlkZ2UuTkVUIHswfSFcIixhbHBoYS54KSk7XHJcblx0XHR9XHJcblx0fVxyXG59IiwidXNpbmcgQmlkb25zLkJhc2U7XHJcbnVzaW5nIFN5c3RlbTtcclxudXNpbmcgU3lzdGVtLkNvbGxlY3Rpb25zLkdlbmVyaWM7XHJcbnVzaW5nIFN5c3RlbS5UZXh0O1xyXG5cclxubmFtZXNwYWNlIEJpZG9ucy5FbGVtZW50c1xyXG57XHJcblx0cHVibGljIGFic3RyYWN0IGNsYXNzIFZpc2libGUgOiBFbGVtZW50LCBJRHJhd2FibGVcclxuXHR7XHJcbiAgICAgICAgcHVibGljIGJvb2wgVmlzaWJpbGl0eSB7IGdldDsgc2V0OyB9XHJcbiAgICAgICAgcHVibGljIExvY2F0aW9uIExvY2F0aW9uIHsgZ2V0OyBzZXQ7IH1cclxuICAgICAgICBwdWJsaWMgQW5jaG9yIEFuY2hvciB7IGdldDsgc2V0OyB9XHJcbiAgICAgICAgcHVibGljIFNpemUgU2l6ZSB7IGdldDsgc2V0OyB9XHJcbiAgICAgICAgcHVibGljIExvY2F0aW9uIEFic29sdXRlIHsgZ2V0IHsgcmV0dXJuIE9yaWdpbi5OZWdhdGl2ZU9mZnNldChBbmNob3IuWCAqIFNpemUuV2lkdGgsIEFuY2hvci5ZICogU2l6ZS5IZWlnaHQpOyB9IHNldCB7IH0gfVxyXG4gICAgXHJcbiAgICAgICAgcHVibGljIExvY2F0aW9uIE9yaWdpblxyXG4gICAgICAgIHtcclxuICAgICAgICAgICAgZ2V0XHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgIGlmIChPd25lciAhPSBudWxsKVxyXG4gICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBMb2NhdGlvbi5PZmZzZXQoKChWaXNpYmxlKSBPd25lcikuQWJzb2x1dGUpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgZWxzZVxyXG4gICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBMb2NhdGlvbi5aZXJvO1xyXG4gICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBzZXQgeyB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBwdWJsaWMgYWJzdHJhY3Qgdm9pZCBEcmF3KCk7XHJcbiAgICAgICAgcHVibGljIGFic3RyYWN0IHZvaWQgRHJhd0F0KGRvdWJsZSB4LCBkb3VibGUgeSk7XHJcblxyXG4jaWYgRUxFQ1RST1xyXG5cdFx0cHVibGljIGR5bmFtaWMgUGl4aU9iamVjdCB7IGdldDsgc2V0OyB9XHJcbiNlbmRpZlxyXG4gICAgfVxyXG59XHJcbiIsInVzaW5nIFN5c3RlbTtcclxudXNpbmcgU3lzdGVtLkNvbGxlY3Rpb25zLkdlbmVyaWM7XHJcbnVzaW5nIFN5c3RlbS5UZXh0O1xyXG5cclxubmFtZXNwYWNlIEJpZG9ucy5Db3JlXHJcbntcclxuXHRwdWJsaWMgY2xhc3MgRmx1eCA6IElVcGRhdGFibGVcclxuXHR7XHJcblx0XHRwdWJsaWMgdm9pZCBVcGRhdGUoZG91YmxlIGR0KVxyXG5cdFx0e1xyXG5cdFx0fVxyXG5cdH1cclxufVxyXG4iLCJ1c2luZyBTeXN0ZW07XHJcbnVzaW5nIFN5c3RlbS5Db2xsZWN0aW9ucy5HZW5lcmljO1xyXG51c2luZyBTeXN0ZW0uVGV4dDtcclxuXHJcbm5hbWVzcGFjZSBCaWRvbnMuQ29yZVxyXG57XHJcblx0cHVibGljIGNsYXNzIFRpbWVyIDogSVVwZGF0YWJsZVxyXG5cdHtcclxuXHRcdHB1YmxpYyB2b2lkIFVwZGF0ZShkb3VibGUgZHQpXHJcblx0XHR7XHJcblx0XHR9XHJcblx0fVxyXG59XHJcbiIsInVzaW5nIEJpZG9ucy5Db3JlO1xyXG51c2luZyBCaWRvbnMuRWxlbWVudHM7XHJcbnVzaW5nIFN5c3RlbTtcclxudXNpbmcgU3lzdGVtLkNvbGxlY3Rpb25zLkdlbmVyaWM7XHJcbnVzaW5nIFN5c3RlbS5UZXh0O1xyXG5cclxubmFtZXNwYWNlIEJpZG9uc1xyXG57XHJcblx0cHVibGljIGludGVyZmFjZSBJQ29udGFpbmVyOiBJRHJhd2FibGVcclxuXHR7XHJcblx0XHRFbGVtZW50c0xpc3QgRWxlbWVudHMoKTtcclxuXHR9XHJcblxyXG5cdHB1YmxpYyBjbGFzcyBFbGVtZW50c0xpc3QgOiBMaXN0PEVsZW1lbnQ+XHJcblx0e1xyXG5cdFx0cHVibGljIElDb250YWluZXIgT3duZXI7XHJcblx0XHRwdWJsaWMgU2NlbmUgU2NlbmU7XHJcblxyXG5cdFx0cHVibGljIG5ldyB2b2lkIEFkZChFbGVtZW50IGl0ZW0pIC8vIFwibmV3XCIgdG8gYXZvaWQgY29tcGlsZXItd2FybmluZ3MsIGJlY2F1c2Ugd2UncmUgaGlkaW5nIGEgbWV0aG9kIGZyb20gYmFzZS1jbGFzc1xyXG5cdFx0e1xyXG5cdFx0XHRpdGVtLlNjZW5lID0gU2NlbmU7XHJcblx0XHRcdGl0ZW0uT3duZXIgPSBPd25lcjtcclxuXHRcdFx0YmFzZS5BZGQoaXRlbSk7XHJcblx0XHR9XHJcblx0fVxyXG59XHJcbiIsInVzaW5nIFN5c3RlbTtcclxudXNpbmcgU3lzdGVtLkNvbGxlY3Rpb25zLkdlbmVyaWM7XHJcbnVzaW5nIFN5c3RlbS5UZXh0O1xyXG5cclxubmFtZXNwYWNlIEJpZG9ucy5FbGVtZW50c1xyXG57XHJcblx0cHVibGljIGNsYXNzIEJveCA6IFZpc2libGUsIElDb250YWluZXJcclxuXHR7XHJcblx0XHRwdWJsaWMgRWxlbWVudHNMaXN0IEVsZW1lbnRzO1xyXG5cclxuXHRcdHB1YmxpYyBCb3goKVxyXG5cdFx0e1xyXG5cdFx0XHRFbGVtZW50cyA9IG5ldyBFbGVtZW50c0xpc3QgeyBPd25lciA9IHRoaXMsIFNjZW5lID0gU2NlbmUgfTtcclxuXHRcdH1cclxuXHJcblx0XHRwdWJsaWMgb3ZlcnJpZGUgdm9pZCBEcmF3KClcclxuXHRcdHtcclxuXHRcdFx0dGhyb3cgbmV3IE5vdEltcGxlbWVudGVkRXhjZXB0aW9uKCk7XHJcblx0XHR9XHJcblxyXG5cdFx0cHVibGljIG92ZXJyaWRlIHZvaWQgRHJhd0F0KGRvdWJsZSB4LCBkb3VibGUgeSlcclxuXHRcdHtcclxuXHRcdFx0dGhyb3cgbmV3IE5vdEltcGxlbWVudGVkRXhjZXB0aW9uKCk7XHJcblx0XHR9XHJcblxyXG5cdFx0RWxlbWVudHNMaXN0IElDb250YWluZXIuRWxlbWVudHMoKVxyXG5cdFx0e1xyXG5cdFx0XHRyZXR1cm4gRWxlbWVudHM7XHJcblx0XHR9XHJcblx0fVxyXG59XHJcbiIsInVzaW5nIFN5c3RlbTtcclxudXNpbmcgU3lzdGVtLkNvbGxlY3Rpb25zO1xyXG51c2luZyBTeXN0ZW0uQ29sbGVjdGlvbnMuR2VuZXJpYztcclxudXNpbmcgU3lzdGVtLkxpbnE7XHJcbnVzaW5nIFN5c3RlbS5UZXh0O1xyXG51c2luZyBCaWRvbnMuQmFzZTtcclxudXNpbmcgQmlkb25zLkVsZW1lbnRzO1xyXG5cclxubmFtZXNwYWNlIEJpZG9ucy5Db3JlXHJcbntcclxuXHRwdWJsaWMgY2xhc3MgU2NlbmUgOiBJQ29udGFpbmVyLCBFdmVudHMuSUV2ZW50YWJsZVxyXG5cdHtcclxuXHRcdHB1YmxpYyBFbmdpbmUgRW5naW5lO1xyXG5cclxuXHRcdEVsZW1lbnRzTGlzdCBJQ29udGFpbmVyLkVsZW1lbnRzKClcclxuXHRcdHtcclxuXHRcdFx0cmV0dXJuIEVsZW1lbnRzO1xyXG5cdFx0fVxyXG5cclxuXHRcdHB1YmxpYyBFbGVtZW50c0xpc3QgRWxlbWVudHMgPSBuZXcgRWxlbWVudHNMaXN0KCk7XHJcblxyXG5cdFx0cHVibGljIFNjZW5lKClcclxuXHRcdHtcclxuXHRcdFx0RWxlbWVudHMgPSBuZXcgRWxlbWVudHNMaXN0IHsgT3duZXIgPSB0aGlzLCBTY2VuZSA9IHRoaXMgfTtcclxuXHRcdH1cclxuXHJcblx0ICAgXHJcblx0XHRwdWJsaWMgdm9pZCBEcmF3KClcclxuXHRcdHtcclxuICAgICAgICAgICAgZm9yZWFjaCAoVmlzaWJsZSBpdGVtIGluIFN5c3RlbS5MaW5xLkVudW1lcmFibGUuV2hlcmU8RWxlbWVudD4oRWxlbWVudHMsKEZ1bmM8RWxlbWVudCxib29sPikodyA9PiB3IGlzIFZpc2libGUpKSlcclxuXHRcdFx0e1xyXG5cdFx0XHRcdGl0ZW0uRHJhdygpO1xyXG5cdFx0XHR9XHJcblx0XHR9XHJcblxyXG5cdFx0cHVibGljIHZvaWQgRHJhd0F0KGRvdWJsZSB4LCBkb3VibGUgeSlcclxuXHRcdHtcclxuXHRcdFx0RHJhdygpO1xyXG5cdFx0fVxyXG5cclxuXHRcdHB1YmxpYyB2b2lkIEtleVByZXNzZWQoKVxyXG5cdFx0e1xyXG5cdFx0XHR0aHJvdyBuZXcgTm90SW1wbGVtZW50ZWRFeGNlcHRpb24oKTtcclxuXHRcdH1cclxuXHJcblx0XHRwdWJsaWMgdm9pZCBVcGRhdGUoZG91YmxlIGR0KVxyXG5cdFx0e1xyXG5cdFx0XHR0aHJvdyBuZXcgTm90SW1wbGVtZW50ZWRFeGNlcHRpb24oKTtcclxuXHRcdH1cclxuXHJcblx0XHRwdWJsaWMgdm9pZCBJbml0KClcclxuXHRcdHtcclxuXHRcdFx0Zm9yZWFjaCAodmFyIGl0ZW0gaW4gRWxlbWVudHMpXHJcblx0XHRcdHtcclxuXHRcdFx0XHRpdGVtLkNyZWF0ZSgpO1xyXG5cdFx0XHR9XHJcblxyXG4jaWYgRUxFQ1RST1xyXG5cdFx0XHRQaXhpT2JqZWN0ID0gRW5naW5lLlBJWElBcHBsaWNhdGlvbi5zdGFnZTtcclxuI2VuZGlmXHJcblx0XHR9XHJcblxyXG4jaWYgRUxFQ1RST1xyXG5cdFx0cHVibGljIGR5bmFtaWMgUGl4aU9iamVjdDtcclxuI2VuZGlmXHJcblx0fVxyXG59XHJcbiIsInVzaW5nIEJpZG9ucy5FbGVtZW50cztcclxudXNpbmcgU3lzdGVtO1xyXG51c2luZyBTeXN0ZW0uQ29sbGVjdGlvbnMuR2VuZXJpYztcclxudXNpbmcgU3lzdGVtLlRleHQ7XHJcblxyXG5uYW1lc3BhY2UgQmlkb25zLkNvcmVcclxue1xyXG5cdHB1YmxpYyBjbGFzcyBPdmVybGF5IDogSUNvbnRhaW5lciwgRXZlbnRzLklFdmVudGFibGVcclxuXHR7XHJcblx0XHRwdWJsaWMgU2NlbmUgU2NlbmU7XHJcblxyXG5cdFx0cHVibGljIEVsZW1lbnRzTGlzdCBFbGVtZW50cztcclxuXHJcblx0XHRwdWJsaWMgT3ZlcmxheShTY2VuZSBzY2VuZSlcclxuXHRcdHtcclxuXHRcdFx0U2NlbmUgPSBzY2VuZTtcclxuXHRcdFx0RWxlbWVudHMgPSBuZXcgRWxlbWVudHNMaXN0IHsgT3duZXIgPSB0aGlzLCBTY2VuZSA9IHNjZW5lIH07XHJcblx0XHR9XHJcblxyXG5cdFx0RWxlbWVudHNMaXN0IElDb250YWluZXIuRWxlbWVudHMoKVxyXG5cdFx0e1xyXG5cdFx0XHRyZXR1cm4gRWxlbWVudHM7XHJcblx0XHR9XHJcblxyXG5cdFx0cHVibGljIHZvaWQgRHJhdygpXHJcblx0XHR7XHJcblx0XHR9XHJcblx0XHRwdWJsaWMgdm9pZCBEcmF3QXQoZG91YmxlIHgsIGRvdWJsZSB5KVxyXG5cdFx0e1xyXG5cdFx0XHREcmF3KCk7XHJcblx0XHR9XHJcblxyXG5cdFx0cHVibGljIHZvaWQgS2V5UHJlc3NlZCgpXHJcblx0XHR7XHJcblx0XHRcdHRocm93IG5ldyBOb3RJbXBsZW1lbnRlZEV4Y2VwdGlvbigpO1xyXG5cdFx0fVxyXG5cclxuXHRcdHB1YmxpYyB2b2lkIFVwZGF0ZShkb3VibGUgZHQpXHJcblx0XHR7XHJcblx0XHRcdHRocm93IG5ldyBOb3RJbXBsZW1lbnRlZEV4Y2VwdGlvbigpO1xyXG5cdFx0fVxyXG5cdH1cclxufVxyXG4iLCJ1c2luZyBTeXN0ZW07XHJcbnVzaW5nIFN5c3RlbS5Db2xsZWN0aW9ucy5HZW5lcmljO1xyXG51c2luZyBTeXN0ZW0uVGV4dDtcclxudXNpbmcgQmlkb25zLkVsZW1lbnRzO1xyXG5cclxubmFtZXNwYWNlIEJpZG9ucy5Db3JlXHJcbntcclxuXHRwdWJsaWMgc2VhbGVkIGNsYXNzIFRvcFJlbmRlciA6IElEcmF3YWJsZSwgSVVwZGF0YWJsZSwgRXZlbnRzLklFdmVudGFibGVcclxuXHR7XHJcblx0XHRwdWJsaWMgVGltZXIgVGltZXIgPSBuZXcgVGltZXIoKTtcclxuXHRcdHB1YmxpYyBGbHV4IEZsdXggPSBuZXcgRmx1eCgpO1xyXG5cclxuXHRcdHB1YmxpYyByZWFkb25seSBFbmdpbmUgRW5naW5lO1xyXG5cdFx0cHVibGljIFRvcFJlbmRlcihFbmdpbmUgZW5naW5lKVxyXG5cdFx0e1xyXG5cdFx0XHRFbmdpbmUgPSBlbmdpbmU7XHJcblxyXG4jaWYgTE9WRVxyXG5cdFx0XHQvKlxyXG5cdFx0XHRbW1xyXG5cdFx0XHRcdGxvdmUuZHJhdyA9IGZ1bmN0aW9uKClcclxuXHRcdFx0XHRcdHRoaXM6RHJhdygpXHJcblx0XHRcdFx0ZW5kXHJcblxyXG5cdFx0XHRcdGxvdmUudXBkYXRlID0gZnVuY3Rpb24oZHQpXHJcblx0XHRcdFx0XHR0aGlzOlVwZGF0ZShkdClcclxuXHRcdFx0XHRlbmRcclxuXHRcdFx0XV0gXHJcblx0XHRcdCovXHJcbiNlbmRpZlxyXG5cclxuXHRcdH1cclxuXHJcblx0XHRwdWJsaWMgU2NlbmUgU2NlbmU7XHJcblx0XHRwdWJsaWMgT3ZlcmxheSBPdmVybGF5O1xyXG5cclxuXHRcdHB1YmxpYyB2b2lkIERyYXcoKVxyXG5cdFx0e1xyXG4jaWYgTE9WRVxyXG5cdFx0XHRTY2VuZSE9bnVsbD9nbG9iYWw6OkJyaWRnZS5TY3JpcHQuRnJvbUxhbWJkYSgoKT0+U2NlbmUuRHJhdygpKTpudWxsO1xyXG5cdFx0XHRPdmVybGF5IT1udWxsP2dsb2JhbDo6QnJpZGdlLlNjcmlwdC5Gcm9tTGFtYmRhKCgpPT5PdmVybGF5LkRyYXcoKSk6bnVsbDtcclxuI2VuZGlmXHJcblx0XHR9XHJcblxyXG5cdFx0cHVibGljIHZvaWQgRHJhd0F0KGRvdWJsZSB4LCBkb3VibGUgeSlcclxuXHRcdHtcclxuXHRcdFx0RHJhdygpO1xyXG5cdFx0fVxyXG5cclxuXHRcdHB1YmxpYyB2b2lkIFVwZGF0ZShkb3VibGUgZHQpXHJcblx0XHR7XHJcblx0XHRcdFRpbWVyIT1udWxsP2dsb2JhbDo6QnJpZGdlLlNjcmlwdC5Gcm9tTGFtYmRhKCgpPT5UaW1lci5VcGRhdGUoZHQpKTpudWxsO1xyXG5cdFx0XHRGbHV4IT1udWxsP2dsb2JhbDo6QnJpZGdlLlNjcmlwdC5Gcm9tTGFtYmRhKCgpPT5GbHV4LlVwZGF0ZShkdCkpOm51bGw7XHJcblx0XHR9XHJcblx0fVxyXG59XHJcbiIsInVzaW5nIEJpZG9ucy5CYXNlO1xyXG51c2luZyBTeXN0ZW07XHJcbnVzaW5nIFN5c3RlbS5Db2xsZWN0aW9ucy5HZW5lcmljO1xyXG51c2luZyBTeXN0ZW0uTGlucTtcclxudXNpbmcgU3lzdGVtLlRleHQ7XHJcbnVzaW5nIFN5c3RlbS5UaHJlYWRpbmcuVGFza3M7XHJcblxyXG5uYW1lc3BhY2UgQmlkb25zLkVsZW1lbnRzXHJcbntcclxuXHRwdWJsaWMgY2xhc3MgQ2lyY2xlIDogVmlzaWJsZVxyXG5cdHtcclxuXHRcdHB1YmxpYyB2aXJ0dWFsIGludCBSYWRpdXMgeyBnZXQ7IHNldDsgfVxyXG5cdFx0cHVibGljIHZpcnR1YWwgQ29sb3IgQ29sb3IgeyBnZXQ7IHNldDsgfVxyXG5cclxuXHRcdHB1YmxpYyBvdmVycmlkZSB2b2lkIENyZWF0ZSgpXHJcblx0XHR7XHJcbiNpZiBFTEVDVFJPXHJcblx0XHRcdHZhciBjaXJjbGUgPSBTY2VuZS5FbmdpbmUuUElYSUhlbHBlci5jcmVhdGUoXCJHcmFwaGljc1wiKTtcclxuXHRcdFx0Y2lyY2xlLmJlZ2luRmlsbChDb2xvci5Db2xvck51bWJlcik7XHJcblx0XHRcdGNpcmNsZS5kcmF3Q2lyY2xlKDAsIDAsIFJhZGl1cyk7XHJcblx0XHRcdGNpcmNsZS54ID0gTG9jYXRpb24uWDtcclxuXHRcdFx0Y2lyY2xlLnkgPSBMb2NhdGlvbi5ZO1xyXG5cdFx0XHRTY2VuZS5FbmdpbmUuUElYSUFwcGxpY2F0aW9uLnN0YWdlLmFkZENoaWxkKGNpcmNsZSk7XHJcbiNlbmRpZlxyXG5cdFx0fVxyXG5cclxuXHRcdHB1YmxpYyBvdmVycmlkZSB2b2lkIERyYXcoKVxyXG5cdFx0e1xyXG4jaWYgTE9WRVxyXG5cdFx0XHQvKlxyXG5cdFx0XHRbW1xyXG5cdFx0XHRcdGxvdmUuZ3JhcGhpY3Muc2V0Q29sb3IodGhpcy5Db2xvci5SLCB0aGlzLkNvbG9yLkcsIHRoaXMuQ29sb3IuQiwgdGhpcy5Db2xvci5BKVxyXG5cdFx0XHRcdGxvdmUuZ3JhcGhpY3MuY2lyY2xlKCdmaWxsJywgdGhpcy5Mb2NhdGlvbi5YLCB0aGlzLkxvY2F0aW9uLlksIHRoaXMuUmFkaXVzKVxyXG5cdFx0XHRdXSBcclxuXHRcdFx0Ki9cclxuI2VuZGlmXHJcblx0XHR9XHJcblxyXG5cdFx0cHVibGljIG92ZXJyaWRlIHZvaWQgRHJhd0F0KGRvdWJsZSB4LCBkb3VibGUgeSlcclxuXHRcdHtcclxuXHRcdFx0dGhyb3cgbmV3IE5vdEltcGxlbWVudGVkRXhjZXB0aW9uKCk7XHJcblx0XHR9XHJcblxuXHRcbnByaXZhdGUgQ29sb3IgX19Qcm9wZXJ0eV9fSW5pdGlhbGl6ZXJfX0NvbG9yPUNvbG9yLldoaXRlO31cclxufVxyXG4iLCJ1c2luZyBCaWRvbnMuQmFzZTtcclxudXNpbmcgU3lzdGVtO1xyXG51c2luZyBTeXN0ZW0uQ29sbGVjdGlvbnMuR2VuZXJpYztcclxudXNpbmcgU3lzdGVtLlRleHQ7XHJcblxyXG5uYW1lc3BhY2UgQmlkb25zLkVsZW1lbnRzXHJcbntcclxuXHRwdWJsaWMgY2xhc3MgTGFiZWwgOiBWaXNpYmxlXHJcblx0e1xyXG4gICAgICAgIHB1YmxpYyBzdHJpbmcgVGV4dCB7IGdldDsgc2V0OyB9XHJcbiAgICAgICAgcHVibGljIEZvbnQgRm9udCB7IGdldDsgc2V0OyB9XHJcblxyXG4gICAgICAgIHB1YmxpYyBDb2xvciBDb2xvciB7IGdldDsgc2V0OyB9XHJcbiAgICAgICAgcHVibGljIG92ZXJyaWRlIHZvaWQgQ3JlYXRlKClcclxuICAgICAgICB7XHJcbiNpZiBFTEVDVFJPXHJcbiAgICAgICAgICAgIHZhciB0ZXh0ID0gU2NlbmUuRW5naW5lLlBJWElIZWxwZXIuY3JlYXRlV2l0aEFyZyhcIlRleHRcIiwgVGV4dCwgbmV3XHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgIHggPSBMb2NhdGlvbi5YLFxyXG4gICAgICAgICAgICAgICAgeSA9IExvY2F0aW9uLlksXHJcbiAgICAgICAgICAgICAgICBmaWxsID0gQ29sb3IuQ29sb3JOdW1iZXJcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIFNjZW5lLkVuZ2luZS5QSVhJQXBwbGljYXRpb24uc3RhZ2UuYWRkQ2hpbGQodGV4dCk7XHJcbiNlbmRpZlxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcHVibGljIG92ZXJyaWRlIHZvaWQgRHJhdygpXHJcbiAgICAgICAge1xyXG4jaWYgTE9WRVxyXG4gICAgICAgICAgICAvKlxyXG5cdFx0XHRbW1xyXG4gICAgICAgICAgICAgICAgbG92ZS5ncmFwaGljcy5zZXRDb2xvcih0aGlzLkNvbG9yLlIsIHRoaXMuQ29sb3IuRywgdGhpcy5Db2xvci5CLCB0aGlzLkNvbG9yLkEpXHJcblx0XHRcdFx0bG92ZS5ncmFwaGljcy5zZXRGb250KHRoaXMuRm9udC5Gb250KVxyXG5cdFx0XHRcdGxvdmUuZ3JhcGhpY3MucHJpbnQodGhpcy5UZXh0LCB0aGlzLk9yaWdpbi5YLCB0aGlzLk9yaWdpbi5ZKTtcclxuXHRcdFx0XV0gXHJcblx0XHRcdCovXHJcbiNlbmRpZlxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcHVibGljIG92ZXJyaWRlIHZvaWQgRHJhd0F0KGRvdWJsZSB4LCBkb3VibGUgeSlcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIHRocm93IG5ldyBOb3RJbXBsZW1lbnRlZEV4Y2VwdGlvbigpO1xyXG4gICAgICAgIH1cclxuXG4gICAgXG5wcml2YXRlIEZvbnQgX19Qcm9wZXJ0eV9fSW5pdGlhbGl6ZXJfX0ZvbnQ9bmV3IEZvbnQoXCJQaXhlbC50dGZcIiwgMTYpO3ByaXZhdGUgQ29sb3IgX19Qcm9wZXJ0eV9fSW5pdGlhbGl6ZXJfX0NvbG9yPUNvbG9yLldoaXRlO31cclxufVxyXG4iLCJ1c2luZyBTeXN0ZW07XHJcbnVzaW5nIFN5c3RlbS5Db2xsZWN0aW9ucy5HZW5lcmljO1xyXG51c2luZyBTeXN0ZW0uVGV4dDtcclxuXHJcbm5hbWVzcGFjZSBCaWRvbnMuRWxlbWVudHNcclxue1xyXG5cdHB1YmxpYyBjbGFzcyBQcm9ncmVzcyA6IFZpc2libGVcclxuXHR7XHJcblx0XHRwdWJsaWMgb3ZlcnJpZGUgdm9pZCBEcmF3KClcclxuXHRcdHtcclxuXHRcdFx0dGhyb3cgbmV3IE5vdEltcGxlbWVudGVkRXhjZXB0aW9uKCk7XHJcblx0XHR9XHJcblxyXG5cdFx0cHVibGljIG92ZXJyaWRlIHZvaWQgRHJhd0F0KGRvdWJsZSB4LCBkb3VibGUgeSlcclxuXHRcdHtcclxuXHRcdFx0dGhyb3cgbmV3IE5vdEltcGxlbWVudGVkRXhjZXB0aW9uKCk7XHJcblx0XHR9XHJcblx0fVxyXG59XHJcbiIsInVzaW5nIFN5c3RlbTtcclxudXNpbmcgU3lzdGVtLkNvbGxlY3Rpb25zLkdlbmVyaWM7XHJcbnVzaW5nIFN5c3RlbS5UZXh0O1xyXG5cclxubmFtZXNwYWNlIEJpZG9ucy5FbGVtZW50c1xyXG57XHJcblx0cHVibGljIGNsYXNzIFRleHQgOiBWaXNpYmxlXHJcblx0e1xyXG5cdFx0cHVibGljIG92ZXJyaWRlIHZvaWQgRHJhdygpXHJcblx0XHR7XHJcblx0XHRcdHRocm93IG5ldyBOb3RJbXBsZW1lbnRlZEV4Y2VwdGlvbigpO1xyXG5cdFx0fVxyXG5cclxuXHRcdHB1YmxpYyBvdmVycmlkZSB2b2lkIERyYXdBdChkb3VibGUgeCwgZG91YmxlIHkpXHJcblx0XHR7XHJcblx0XHRcdHRocm93IG5ldyBOb3RJbXBsZW1lbnRlZEV4Y2VwdGlvbigpO1xyXG5cdFx0fVxyXG5cdH1cclxufVxyXG4iLCIvL3VzaW5nIE5ld3RvbnNvZnQuSnNvbjtcclxudXNpbmcgU3lzdGVtO1xyXG51c2luZyBTeXN0ZW0uQ29sbGVjdGlvbnMuR2VuZXJpYztcclxudXNpbmcgU3lzdGVtLklPO1xyXG51c2luZyBTeXN0ZW0uTGlucTtcclxudXNpbmcgU3lzdGVtLlRleHQ7XHJcbnVzaW5nIFN5c3RlbS5UaHJlYWRpbmcuVGFza3M7XHJcblxyXG5uYW1lc3BhY2UgQmlkb25zLkNvcmVcclxue1xyXG5cdHB1YmxpYyBjbGFzcyBPbGRTY2VuZSA6IFNjZW5lXHJcblx0e1xyXG5cdFx0cHVibGljIGR5bmFtaWMgU291cmNlO1xyXG5cclxuXHRcdHB1YmxpYyBPbGRTY2VuZSgpIHsgfVxyXG5cclxuXHRcdHB1YmxpYyBPbGRTY2VuZShzdHJpbmcgc2NlbmVQYXRoKSA6IGJhc2UoKVxyXG5cdFx0e1xyXG5cdFx0XHQvL3ZhciBzY2VuZUNvbnRlbnQgPSBGaWxlLlJlYWRBbGxUZXh0KHNjZW5lUGF0aCk7XHJcblx0XHRcdC8vU291cmNlID0gSnNvbkNvbnZlcnQuRGVzZXJpYWxpemVPYmplY3Q8ZHluYW1pYz4oc2NlbmVDb250ZW50KTtcclxuXHRcdH1cclxuXHR9XHJcbn1cclxuIiwidXNpbmcgQmlkb25zLkJhc2U7XHJcbnVzaW5nIEJpZG9ucy5Db3JlO1xyXG51c2luZyBCaWRvbnMuRWxlbWVudHM7XHJcbnVzaW5nIFN5c3RlbTtcclxudXNpbmcgU3lzdGVtLkNvbGxlY3Rpb25zLkdlbmVyaWM7XHJcbnVzaW5nIFN5c3RlbS5MaW5xO1xyXG51c2luZyBTeXN0ZW0uVGV4dDtcclxudXNpbmcgU3lzdGVtLlRocmVhZGluZy5UYXNrcztcclxuXHJcbm5hbWVzcGFjZSBNeUdhbWVcclxue1xyXG5cdGNsYXNzIFNpbXBsZVNjZW5lIDogU2NlbmVcclxuXHR7XHJcblx0XHRwdWJsaWMgU2ltcGxlU2NlbmUoKVxyXG5cdFx0e1xyXG5cdFx0XHRFbGVtZW50cy5BZGQobmV3IENpcmNsZSB7IExvY2F0aW9uID0gKExvY2F0aW9uKSBuZXdbXSB7IDAsIDAgfSwgUmFkaXVzID0gMjUsIENvbG9yID0gQ29sb3IuQmx1ZSB9KTtcclxuICAgICAgICAgICAgRWxlbWVudHMuQWRkKG5ldyBDaXJjbGUgeyBMb2NhdGlvbiA9IChMb2NhdGlvbikgbmV3W10geyAxMDAsIDE1MCB9LCBSYWRpdXMgPSAzNSwgQ29sb3IgPSBDb2xvci5SZWQgfSk7XHJcbiAgICAgICAgICAgIEVsZW1lbnRzLkFkZChuZXcgTGFiZWwgeyBUZXh0ID0gXCJHYW1lIE92ZXIhXCIsIENvbG9yID0gQ29sb3IuUmVkIH0pO1xyXG4gICAgICAgIH1cclxuXHR9XHJcbn1cclxuIl0KfQo=
