"use strict";

const electron = require("electron");
// Handle creating/removing shortcuts on Windows when installing/uninstalling.
// if (require('electron-squirrel-startup')) { // eslint-disable-line global-require
// app.quit();
// }
// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.

let mainWindow;
let beApp;

const createWindow = () => {
    electron.ipcMain.on('setBeApplication', (event, be_app) => {
        beApp = be_app;
    });
    // Create the browser window.
    mainWindow = new electron.BrowserWindow({
		webPreferences: {
			nodeIntegration: true,
			webviewTag: true,
		},
        width: 800,
        height: 600,
        show: false,
        title: 'Bidons WAE',
    });
    let splash = new electron.BrowserWindow({ width: 856, height: 571, transparent: true, frame: false, alwaysOnTop: true });
    splash.loadURL(`file://${__dirname}/splash.html`);
    // and load the index.html of the app.
    mainWindow.loadURL(`file://${__dirname}/index.html`);
    // Open the DevTools.
    // mainWindow.webContents.openDevTools();
    // Emitted when the window is closed.
    mainWindow.on('closed', () => {
        // Dereference the window object, usually you would store windows
        // in an array if your app supports multi windows, this is the time
        // when you should delete the corresponding element.
        // for (var tab of beApp.tabs.tabs)
            // if (tab.webview && tab.webview.remove)
                // tab.webview.remove();
        mainWindow = null;
    });
    mainWindow.once('ready-to-show', () => {
        setTimeout(() => {
            splash.destroy();
            mainWindow.show();
        }, 25);
    });
};
// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
electron.app.on('ready', createWindow);
// Quit when all windows are closed.
electron.app.on('window-all-closed', () => {
    // On OS X it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform !== 'darwin') {
        electron.app.quit();
    }
});
electron.app.on('activate', () => {
    // On OS X it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (mainWindow === null) {
        createWindow();
    }
});
// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and import them here.
