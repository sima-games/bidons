﻿using System;

namespace Bidons_B
{
	public class App
	{
		public class Alpha
		{
			public int x = 5;
			public int y = 7;
		}

		public static void Main()
		{
			dynamic alpha = new Alpha();

			Console.WriteLine($"Welcome to Bridge.NET {alpha.x}!");
		}
	}
}