-- Generated by CSharp.lua Compiler
local System = System
System.namespace("Bidons.Events", function (namespace)
  namespace.interface("IEventable", function ()
    return {
      __inherits__ = function (out)
        return {
          out.Bidons.Events.IContextEvents,
          out.Bidons.Events.IEngineEvents,
          out.Bidons.Events.IKeyEvents,
          out.Bidons.Events.IMouseEvents
        }
      end
    }
  end)
end)
