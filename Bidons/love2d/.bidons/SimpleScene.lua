-- Generated by CSharp.lua Compiler
local System = System
local ArrayInt32 = System.Array(System.Int32)
local BidonsBase
local BidonsElements
System.import(function (out)
  BidonsBase = Bidons.Base
  BidonsElements = Bidons.Elements
end)
System.namespace("MyGame", function (namespace)
  namespace.class("SimpleScene", function (namespace)
    local __ctor__
    __ctor__ = function (this)
      System.base(this).__ctor__(this)
      local default = BidonsElements.Circle()
      default.Location = BidonsBase.Location.op_Implicit2(ArrayInt32(0, 0))
      default:setRadius(25)
      default:setColor(BidonsBase.Color.Blue:__clone__())
      this.Elements1:Add1(default)
      local extern = BidonsElements.Circle()
      extern.Location = BidonsBase.Location.op_Implicit2(ArrayInt32(100, 150))
      extern:setRadius(35)
      extern:setColor(BidonsBase.Color.Red:__clone__())
      this.Elements1:Add1(extern)
      local ref = BidonsElements.Label()
      ref.Text = "Game Over!"
      ref.Color = BidonsBase.Color.Red:__clone__()
      this.Elements1:Add1(ref)
    end
    return {
      __inherits__ = function (out)
        return {
          out.Bidons.Core.Scene
        }
      end,
      __ctor__ = __ctor__
    }
  end)
end)
