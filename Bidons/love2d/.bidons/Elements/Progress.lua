-- Generated by CSharp.lua Compiler
local System = System
System.namespace("Bidons.Elements", function (namespace)
  namespace.class("Progress", function (namespace)
    local Draw, DrawAt
    Draw = function (this)
      System.throw(System.NotImplementedException())
    end
    DrawAt = function (this, x, y)
      System.throw(System.NotImplementedException())
    end
    return {
      __inherits__ = function (out)
        return {
          out.Bidons.Elements.Visible
        }
      end,
      Draw = Draw,
      DrawAt = DrawAt
    }
  end)
end)
