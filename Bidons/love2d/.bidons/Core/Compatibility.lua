-- Generated by CSharp.lua Compiler
local System = System
local BidonsBase
local BidonsCore
System.import(function (out)
  BidonsBase = Bidons.Base
  BidonsCore = Bidons.Core
end)
System.namespace("Bidons.Core", function (namespace)
  namespace.class("Compatibility", function (namespace)
    local EntrustOldBscn
    namespace.class("Box", function (namespace)
      local getBackColor, setBackColor, Draw, Create, __ctor__
      __ctor__ = function (this, source)
        this._color = BidonsBase.Color.White:__clone__()
        System.base(this).__ctor__(this)
        if source.Color then
          this._color = source.Color
        end
        if source:get("@" + source.Color) then
        end
      end
      getBackColor = function (this)
        return this._color:__clone__()
      end
      setBackColor = function (this, value)
        this._color = value:__clone__()
      end
      Draw = function (this)
      end
      Create = function (this)
      end
      return {
        __inherits__ = function (out)
          return {
            out.Bidons.Elements.Box
          }
        end,
        getBackColor = getBackColor,
        setBackColor = setBackColor,
        Draw = Draw,
        Create = Create,
        __ctor__ = __ctor__
      }
    end)
    EntrustOldBscn = function (konstabel, scenePath)
      local scene = BidonsCore.Scene()
      konstabel:Entrust(scene)
    end
    return {
      EntrustOldBscn = EntrustOldBscn
    }
  end)
end)
