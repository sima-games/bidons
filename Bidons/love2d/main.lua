package.path = package.path .. ';.bidons/?.lua'

local gameData = {
	id = 'Shame of Thrones',
	version = 'b3/18e',
	context = {
		day = 25
	}
}

local engine = require('bidons')(gameData.id, gameData.version, gameData.context)

-- engine.Konstabel:LoadScenes('scenes')

local scene = MyGame:SimpleScene()
engine.Konstabel:Entrust(scene)

