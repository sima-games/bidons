﻿using Bidons.Base;
using Bidons.Elements;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bidons.Core
{
	[Obsolete]
	public static class Compatibility
	{
		public static void EntrustOldBscn(this Konstabel konstabel, string scenePath)
		{
			Scene scene = new Scene();
			konstabel.Entrust(scene);
		}

		public class Box : Elements.Box
		{
			public Color _color = Color.White;
			public Color BackColor
			{
				get { return _color; }
				set
				{
					_color = value;
				}
			}

			public Box(dynamic source)
            {
				if (source.Color) _color = source.Color;
				if (source["@" + source.Color]) {
					
				}
				
			}
			public Box() { }

			public override void Draw()
			{
				
			}

			public override void Create()
			{
#if ELECTRO
				PixiObject = Scene.Engine.PIXIHelper.create("Container");

				PixiObject.x = Location.X;
				PixiObject.y = Location.Y;
				//Owner.Pix.addChild(PixiObject);
#endif
			}
		}
	}
}
