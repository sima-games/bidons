﻿using System;
using System.Collections.Generic;
using System.Text;
using Bidons.Elements;

namespace Bidons.Core
{
	public sealed class TopRender : IDrawable, IUpdatable, Events.IEventable
	{
		public Timer Timer = new Timer();
		public Flux Flux = new Flux();

		public readonly Engine Engine;
		public TopRender(Engine engine)
		{
			Engine = engine;

#if LOVE
			/*
			[[
				love.draw = function()
					this:Draw()
				end

				love.update = function(dt)
					this:Update(dt)
				end
			]] 
			*/
#endif

		}

		public Scene Scene;
		public Overlay Overlay;

		public void Draw()
		{
#if LOVE
			Scene?.Draw();
			Overlay?.Draw();
#endif
		}

		public void DrawAt(double x, double y)
		{
			Draw();
		}

		public void Update(double dt)
		{
			Timer?.Update(dt);
			Flux?.Update(dt);
		}
	}
}
