﻿#if ELECTRO
using Bridge;
using Bridge.Html5;
#endif

using System;

namespace Bidons.Core
{
	public class Engine
	{
		public readonly Konstabel Konstabel;
		public readonly TransManager TransManager;
		public readonly SaveLoader SaveLoader;
		public readonly StackLog StackLog;
		public readonly TopRender TopRender;
		public readonly FastDraw FastDraw;
		public dynamic Context;
		public string GameId;
		public string GameVersion;

#if ELECTRO
		public dynamic PIXIHelper = Script.Get("PIXIHelper");
		public dynamic PIXIApplication;
#endif

		public Renderer CurrentRenderer = Renderer.Electron;
		public enum Renderer
		{
			Electron, Love2D
		}


		public Engine(string gameId, string gameVersion, dynamic context)
		{
			GameId = gameId;
			GameVersion = gameVersion;
			Konstabel = new Konstabel(this);
			TransManager = new TransManager(this);
			SaveLoader = new SaveLoader(this);
			StackLog = new StackLog(this);
			TopRender = new TopRender(this);
			FastDraw = new FastDraw(this);
			Context = context;

#if ELECTRO
			PIXIApplication = PIXIHelper.create("Application", new {
				width = 500,
				height = 500,
				view = Document.GetElementById("main")
			});
#endif
		}
	}
}
