﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bidons.Base;
using Bidons.Elements;

namespace Bidons.Core
{
	public class Scene : IContainer, Events.IEventable
	{
		public Engine Engine;

		ElementsList IContainer.Elements()
		{
			return Elements;
		}

		public ElementsList Elements = new ElementsList();

		public Scene()
		{
			Elements = new ElementsList { Owner = this, Scene = this };
		}

	   
		public void Draw()
		{
            foreach (Visible item in Elements.Where(w => w is Visible))
			{
				item.Draw();
			}
		}

		public void DrawAt(double x, double y)
		{
			Draw();
		}

		public void KeyPressed()
		{
			throw new NotImplementedException();
		}

		public void Update(double dt)
		{
			throw new NotImplementedException();
		}

		public void Init()
		{
			foreach (var item in Elements)
			{
				item.Create();
			}

#if ELECTRO
			PixiObject = Engine.PIXIApplication.stage;
#endif
		}

#if ELECTRO
		public dynamic PixiObject;
#endif
	}
}
