﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bidons.Core
{
	public class Konstabel
	{
		public readonly Engine Engine;
		public Konstabel(Engine engine)
		{
			Engine = engine;
		}

		public void Entrust(Scene scene)
		{
			scene.Engine = Engine;
			Engine.TopRender.Scene = scene;
			scene.Init();
		}

		public void Entrust(string sceneName)
		{

		}
	}
}
