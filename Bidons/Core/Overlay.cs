﻿using Bidons.Elements;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bidons.Core
{
	public class Overlay : IContainer, Events.IEventable
	{
		public Scene Scene;

		public ElementsList Elements;

		public Overlay(Scene scene)
		{
			Scene = scene;
			Elements = new ElementsList { Owner = this, Scene = scene };
		}

		ElementsList IContainer.Elements()
		{
			return Elements;
		}

		public void Draw()
		{
		}
		public void DrawAt(double x, double y)
		{
			Draw();
		}

		public void KeyPressed()
		{
			throw new NotImplementedException();
		}

		public void Update(double dt)
		{
			throw new NotImplementedException();
		}
	}
}
