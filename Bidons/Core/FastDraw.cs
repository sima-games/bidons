﻿using Bidons.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bidons.Core
{
	public class FastDraw
	{
		public readonly Engine Engine;
		public FastDraw(Engine engine)
		{
			Engine = engine;
		}

		public void DrawImage(Image image) {
			#if __CSharpLua__
			/*[[
			love.graphics.print('hello, word')
			]]*/  
			#else
			Console.WriteLine("hello, word");
			#endif
		}

		public void DrawText(Text text)
		{

		}
	}
}
