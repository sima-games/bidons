﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bidons
{
	public interface IUpdatable
	{
		void Update(double dt);
	}
}
