﻿using Bidons.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bidons.Elements
{
	public abstract class Element
	{
		public Scene Scene { get; set; }
		public IContainer Owner { get; set; }
		public virtual void Create() { }
	}
}
