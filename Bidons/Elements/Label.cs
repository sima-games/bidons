﻿using Bidons.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bidons.Elements
{
	public class Label : Visible
	{
        public string Text { get; set; }
        public Font Font { get; set; } = new Font("Pixel.ttf", 16);

        public Color Color { get; set; } = Color.White;
        public override void Create()
        {
#if ELECTRO
            var text = Scene.Engine.PIXIHelper.createWithArg("Text", Text, new
            {
                x = Location.X,
                y = Location.Y,
                fill = Color.ColorNumber
            });
            Scene.Engine.PIXIApplication.stage.addChild(text);
#endif
        }

        public override void Draw()
        {
#if LOVE
            /*
			[[
                love.graphics.setColor(this.Color.R, this.Color.G, this.Color.B, this.Color.A)
				love.graphics.setFont(this.Font.Font)
				love.graphics.print(this.Text, this.Origin.X, this.Origin.Y);
			]] 
			*/
#endif
        }

        public override void DrawAt(double x, double y)
        {
            throw new NotImplementedException();
        }
    }
}
