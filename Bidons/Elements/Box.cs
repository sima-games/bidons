﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Bidons.Base;

namespace Bidons.Elements
{
    public class Box : Visible, IContainer
    {
        public ElementsList Elements;

        public Color BindingColor;
        public bool BindingVisible;
        public Color BackgroundColor;
        public Image BackgroundImage;

        public Box()
        {
            Elements = new ElementsList { Owner = this, Scene = Scene };
        }

        public override void Draw()
        {
            if (BackgroundImage != null)
            {
                BackgroundImage.DrawAt(Absolute.X, Absolute.Y);
            }

            foreach (Visible element in Elements.Where(e => e is Visible))
            {
                element.Draw();
            }
        }

        public override void DrawAt(double x, double y)
        {
            throw new NotImplementedException();
        }

        ElementsList IContainer.Elements()
        {
            return Elements;
        }
    }
}
