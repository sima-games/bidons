﻿using Bidons.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bidons.Elements
{
	public class Circle : Visible
	{
		public virtual int Radius { get; set; }
		public virtual Color Color { get; set; } =  Color.White;

		public override void Create()
		{
#if ELECTRO
			var circle = Scene.Engine.PIXIHelper.create("Graphics");
			circle.beginFill(Color.ColorNumber);
			circle.drawCircle(0, 0, Radius);
			circle.x = Location.X;
			circle.y = Location.Y;
			Scene.Engine.PIXIApplication.stage.addChild(circle);
#endif
		}

		public override void Draw()
		{
#if LOVE
			/*
			[[
				love.graphics.setColor(this.Color.R, this.Color.G, this.Color.B, this.Color.A)
				love.graphics.circle('fill', this.Location.X, this.Location.Y, this.Radius)
			]] 
			*/
#endif
		}

		public override void DrawAt(double x, double y)
		{
			throw new NotImplementedException();
		}
	}
}
