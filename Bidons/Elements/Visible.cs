﻿using Bidons.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bidons.Elements
{
	public abstract class Visible : Element, IDrawable
	{
        public bool Visibility { get; set; }
        public Location Location { get; set; }
        public Anchor Anchor { get; set; }
        public Size Size { get; set; }
        public Location Absolute { get { return Origin.NegativeOffset(Anchor.X * Size.Width, Anchor.Y * Size.Height); } set { } }
    
        public Location Origin
        {
            get
            {
                if (Owner != null)
                {
                    return Location.Offset(((Visible) Owner).Absolute);
                }
                else
                {
                    return Location.Zero;
                };
            }
            set { }
        }

        public abstract void Draw();
        public abstract void DrawAt(double x, double y);

#if ELECTRO
		public dynamic PixiObject { get; set; }
#endif
    }
}
