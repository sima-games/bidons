﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bidons.Base
{
	public struct Location
	{
        public static Location Zero = new Location(0, 0);

		public double X;
		public double Y;
		public Location(double x, double y) {
			X = x;
			Y = y;
		}

        public Location Offset(double offsetX, double offsetY)
        {
            return new Location(X + offsetX, Y + offsetY);
        }

        public Location Offset(Location offset)
        {
            return new Location(X + offset.X, Y + offset.Y);
        }

        public Location NegativeOffset(double offsetX, double offsetY)
        {
            return new Location(X - offsetX, Y - offsetY);
        }

        public Location NegativeOffset(Location offset)
        {
            return new Location(X - offset.X, Y - offset.Y);
        }

        public static implicit operator Location(Tuple<double, double> tuple)
			=> new Location(tuple.Item1, tuple.Item2);

		public static implicit operator Location(Tuple<int, int> tuple)
		   => new Location(tuple.Item1, tuple.Item2);

		public static implicit operator Location(int[] array)
		{
			if (array.Length != 2) throw new InvalidCastException("Cannot convert int[] to Location, wrong array length");
			return new Location(array[0], array[1]);
		}

		public static implicit operator Location(double[] array)
		{
			if (array.Length != 2) throw new InvalidCastException("Cannot convert double[] to Location, wrong array length");
			return new Location(array[0], array[1]);
		}
	}
}
