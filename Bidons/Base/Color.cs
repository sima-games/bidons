﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bidons.Base
{
	public struct Color
	{
		public float R;
		public float G;
		public float B;
		public float A;
		public int ColorNumber;

		public Color(string stringHex) : this(Convert.ToInt32(stringHex.Replace("#", ""), 16))
		{
		}

		public Color(int colorNumber)
		{
			ColorNumber = colorNumber;
			byte[] values = BitConverter.GetBytes(colorNumber);
			if (!BitConverter.IsLittleEndian) Array.Reverse(values);
			R = values[2];
			G = values[1];
			B = values[0];
			R /= 255;
			G /= 255;
			B /= 255;
			A = 1;
		}

		public static implicit operator Color(int colorNumber)
		   => new Color(colorNumber);

		public static Color Red = new Color(0xff0000);
		public static Color Green = new Color(0x00ff00);
		public static Color Blue = new Color(0x0000ff);
		public static Color Black = new Color(0x000000);
		public static Color White = new Color(0xffffff);
	}
}
