﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bidons.Base
{
	public struct Size
	{
		public double Width;
		public double Height;

		public Size(double width, double height)
		{
			Width = width;
			Height = height;
		}

		public static implicit operator Size(Tuple<double, double> tuple)
		  => new Size(tuple.Item1, tuple.Item2);

		public static implicit operator Size(Tuple<int, int> tuple)
		   => new Size(tuple.Item1, tuple.Item2);

		public static implicit operator Size(int[] array)
		{
			if (array.Length != 2) throw new InvalidCastException("Cannot convert int[] to Size, wrong array length");
			return new Size(array[0], array[1]);
		}

		public static implicit operator Size(double[] array)
		{
			if (array.Length != 2) throw new InvalidCastException("Cannot convert double[] to Size, wrong array length");
			return new Size(array[0], array[1]);
		}
	}
}
