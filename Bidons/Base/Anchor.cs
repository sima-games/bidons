﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bidons.Base
{
	public struct Anchor
	{
		public double X;
		public double Y;
		public Anchor(double x, double y)
		{
			X = x;
			Y = y;
		}

		public static implicit operator Anchor(Tuple<double, double> tuple)
			=> new Anchor(tuple.Item1, tuple.Item2);

		public static implicit operator Anchor(Tuple<int, int> tuple)
		   => new Anchor(tuple.Item1, tuple.Item2);

		public static implicit operator Anchor(int[] array)
		{
			if (array.Length != 2) throw new InvalidCastException("Cannot convert int[] to Anchor, wrong array length");
			return new Anchor(array[0], array[1]);
		}

		public static implicit operator Anchor(double[] array)
		{
			if (array.Length != 2) throw new InvalidCastException("Cannot convert double[] to Anchor, wrong array length");
			return new Anchor(array[0], array[1]);
		}
	}
}
