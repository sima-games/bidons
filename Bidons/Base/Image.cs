﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bidons.Base
{
    public class Image : IDrawable
    {
        public string Path;

        public Image()
        {
#if LOVE
            /*
			[[
				this.Image = love.graphics.newImage(this.Path)
			]] 
			*/
#endif
#if ELECTRO

#endif
        }

        public void Draw()
        {
            throw new NotImplementedException();
        }

        public void DrawAt(double x, double y)
        {
#if LOVE
            /*
			[[
				love.graphics.draw(this.Image, x, y)
			]] 
			*/
#endif
        }
    }
}
