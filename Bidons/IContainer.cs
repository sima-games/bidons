﻿using Bidons.Core;
using Bidons.Elements;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bidons
{
	public interface IContainer: IDrawable
	{
		ElementsList Elements();
	}

	public class ElementsList : List<Element>
	{
		public IContainer Owner;
		public Scene Scene;

		public new void Add(Element item) // "new" to avoid compiler-warnings, because we're hiding a method from base-class
		{
			item.Scene = Scene;
			item.Owner = Owner;
			base.Add(item);
		}
	}
}
