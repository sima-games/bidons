﻿using System;
using System.Diagnostics;

namespace BidonsRunner
{
	class Program
	{
		static void Main(string[] args)
		{
#if ELECTRO
			var process = Process.Start(new ProcessStartInfo {
				FileName = @"cmd.exe",
				Arguments = @"/c ""yarn start""",
				WorkingDirectory = @"..\..\..\..\Bidons\electron",
				CreateNoWindow = true
			});

			process.WaitForExit();

#else
			Process.Start(new ProcessStartInfo
			{
				FileName = "cmd.exe",
				Arguments = @"/c ""x64\love .""",
				WorkingDirectory = @"..\..\..\..\Bidons\love2d",
				CreateNoWindow = true
			});
#endif
		}
	}
}
